from PySide.QtCore import *
from PySide.QtGui import *
import hashlib


class QPushButtonSquare(QPushButton):
    def __init__(self, text):
        super().__init__(text)
        self.setFocusPolicy(Qt.NoFocus)
        self.setFixedSize(50, 50)


class PinDialog(QDialog):
    def __init__(self):
        super().__init__()

        self.setWindowTitle('PIN')

        self.txtPassword = QLineEdit()
        self.txtPassword.setEchoMode(QLineEdit.Password)
        self.txtPassword.setFocusPolicy(Qt.NoFocus)
        self.txtPassword.setAlignment(Qt.AlignCenter)

        self.btn1 = QPushButtonSquare('1')
        self.btn2 = QPushButtonSquare('2')
        self.btn3 = QPushButtonSquare('3')
        self.btn4 = QPushButtonSquare('4')
        self.btn5 = QPushButtonSquare('5')
        self.btn6 = QPushButtonSquare('6')
        self.btn7 = QPushButtonSquare('7')
        self.btn8 = QPushButtonSquare('8')
        self.btn9 = QPushButtonSquare('9')
        self.btn0 = QPushButtonSquare('0')

        self.btnBackspace = QPushButtonSquare('⌫')
        self.btnOK = QPushButtonSquare('⏎')

        self.btn1.clicked.connect(self.addNum)
        self.btn2.clicked.connect(self.addNum)
        self.btn3.clicked.connect(self.addNum)
        self.btn4.clicked.connect(self.addNum)
        self.btn5.clicked.connect(self.addNum)
        self.btn6.clicked.connect(self.addNum)
        self.btn7.clicked.connect(self.addNum)
        self.btn8.clicked.connect(self.addNum)
        self.btn9.clicked.connect(self.addNum)
        self.btn0.clicked.connect(self.addNum)
        self.btnBackspace.clicked.connect(lambda: self.txtPassword.setText(self.txtPassword.text()[:-1]))
        self.btnOK.clicked.connect(lambda: self.close())

        layout = QGridLayout()
        layout.addWidget(self.txtPassword, 0, 1, 1, 3)
        layout.addWidget(self.btn1, 1, 1)
        layout.addWidget(self.btn2, 1, 2)
        layout.addWidget(self.btn3, 1, 3)
        layout.addWidget(self.btn4, 2, 1)
        layout.addWidget(self.btn5, 2, 2)
        layout.addWidget(self.btn6, 2, 3)
        layout.addWidget(self.btn7, 3, 1)
        layout.addWidget(self.btn8, 3, 2)
        layout.addWidget(self.btn9, 3, 3)
        layout.addWidget(self.btnBackspace, 4, 1)
        layout.addWidget(self.btn0, 4, 2)
        layout.addWidget(self.btnOK, 4, 3)

        self.setLayout(layout)

    def addNum(self):
        self.txtPassword.setText(self.txtPassword.text() + self.sender().text())

    def getSha1PinHash(self):
        self.exec_()
        return hashlib.sha1(self.txtPassword.text().encode('utf-8')).hexdigest()
