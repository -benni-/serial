from UI.CustomWidgets import *


class AxisSpeedWidget(QGroupBox):
    def __init__(self, FPDM):
        super().__init__()
        self.FPDM = FPDM
        self.setupUI()

        self.btnDecreaseSpeed10.clicked.connect(lambda: self.FPDM.changeSpeedFactor(-0.1))
        self.btnDecreaseSpeed1.clicked.connect(lambda: self.FPDM.changeSpeedFactor(-0.01))
        self.btnIncreaseSpeed1.clicked.connect(lambda: self.FPDM.changeSpeedFactor(0.01))
        self.btnIncreaseSpeed10.clicked.connect(lambda: self.FPDM.changeSpeedFactor(0.1))
        self.FPDM.speedFactorChanged.connect(lambda factor: self.lblSpeedPercent.setText('{0}%'.format(str(round(factor * 100)))))

    def setupUI(self):
        self.setTitle('Achsgeschwindigkeit')

        self.btnDecreaseSpeed10 = QPushButtonExpanding('-10%')
        self.btnDecreaseSpeed1 = QPushButtonExpanding('-1%')
        self.lblSpeedPercent = QLabelSunkenCentered('100%')
        self.lblSpeedPercent.setMinimumWidth(50)
        self.btnIncreaseSpeed1 = QPushButtonExpanding('+1%')
        self.btnIncreaseSpeed10 = QPushButtonExpanding('+10%')

        lytAxisSpeed = QHBoxLayout()
        lytAxisSpeed.addWidget(self.btnDecreaseSpeed10)
        lytAxisSpeed.addWidget(self.btnDecreaseSpeed1)
        lytAxisSpeed.addWidget(self.lblSpeedPercent)
        lytAxisSpeed.addWidget(self.btnIncreaseSpeed1)
        lytAxisSpeed.addWidget(self.btnIncreaseSpeed10)

        self.setLayout(lytAxisSpeed)
