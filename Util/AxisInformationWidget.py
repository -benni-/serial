from UI.CustomWidgets import *
import logging


class AxisInformationWidget(QGroupBox):
    def __init__(self, FPDM):
        super().__init__()
        self.setupUI()
        self.FPDM = FPDM

        self.axisXTargetPosition = None
        self.axisZTargetPosition = None

        self.FPDM.AxisX.currentState.connect(self.receivedAxisXState)
        self.FPDM.AxisX.demandPosition.connect(self.receivedAxisXDemandPosition)
        self.FPDM.AxisX.encoderPosition.connect(self.receivedAxisXEncoderPosition)
        self.FPDM.AxisX.targetPosition.connect(self.receivedAxisXTargetPosition)

        self.FPDM.AxisC.currentState.connect(self.receivedAxisCState)

        self.FPDM.AxisZ.currentState.connect(self.receivedAxisZState)
        self.FPDM.AxisZ.demandPosition.connect(self.receivedAxisZDemandPosition)
        self.FPDM.AxisZ.encoderPosition.connect(self.receivedAxisZEncoderPosition)
        self.FPDM.AxisZ.targetPosition.connect(self.receivedAxisZTargetPosition)

    def receivedAxisXDemandPosition(self, demandPosition):
        self.axisXDemandPosition = demandPosition
        self.lblAxisXDemandPosition.setText('{0:.3f}'.format(demandPosition / self.FPDM.AxisX.stepsPerMM))

    def receivedAxisZDemandPosition(self, demandPosition):
        self.axisZDemandPosition = demandPosition
        self.lblAxisZDemandPosition.setText('{0:.3f}'.format(demandPosition / self.FPDM.AxisZ.stepsPerMM))

    def receivedAxisXEncoderPosition(self, encoderPosition):
        self.axisXEncoderPosition = encoderPosition
        self.lblAxisXEncoderPosition.setText('{0:.3f}'.format(encoderPosition / self.FPDM.AxisX.stepsPerMM))
        self.lblAxisXContouringError.setText('{0:.3f}'.format((self.axisXDemandPosition - encoderPosition) / self.FPDM.AxisX.stepsPerMM))
        if self.axisXTargetPosition:
            self.lblAxisXDistanceToGo.setText('{0:.3f}'.format((self.axisXTargetPosition - encoderPosition) / self.FPDM.AxisX.stepsPerMM))

    def receivedAxisZEncoderPosition(self, encoderPosition):
        self.axisZEncoderPosition = encoderPosition
        self.lblAxisZEncoderPosition.setText('{0:.3f}'.format(encoderPosition / self.FPDM.AxisZ.stepsPerMM))
        self.lblAxisZContouringError.setText('{0:.3f}'.format((self.axisZDemandPosition - encoderPosition) / self.FPDM.AxisZ.stepsPerMM))
        if self.axisZTargetPosition:
            self.lblAxisZDistanceToGo.setText('{0:.3f}'.format((self.axisZTargetPosition - encoderPosition) / self.FPDM.AxisZ.stepsPerMM))

    def receivedAxisXTargetPosition(self, targetPosition):
        self.axisXTargetPosition = targetPosition
        self.lblAxisXTargetPosition.setText('{0:.3f}'.format(targetPosition / self.FPDM.AxisX.stepsPerMM))

    def receivedAxisZTargetPosition(self, targetPosition):
        self.axisZTargetPosition = targetPosition
        self.lblAxisZTargetPosition.setText('{0:.3f}'.format(targetPosition / self.FPDM.AxisZ.stepsPerMM))

    def receivedAxisXState(self, statusByte):
        self.chkAxisXReady.setChecked(statusByte & 1)
        self.lblAxisXStatusByte.setText(str(statusByte))

    def receivedAxisCState(self, statusByte):
        self.chkAxisCReady.setChecked(statusByte & 1)
        self.lblAxisCStatusByte.setText(str(statusByte))

    def receivedAxisZState(self, statusByte):
        self.chkAxisZReady.setChecked(statusByte & 1)
        self.lblAxisZStatusByte.setText(str(statusByte))

    def setupUI(self):
        self.lblPositionText = QLabelAlignedRight()
        self.lblEncoderText = QLabelAlignedRight()
        self.lblContouringErrorText = QLabelAlignedRight()
        self.lblTargetPositionText = QLabelAlignedRight()
        self.lblRemainingDistanceText = QLabelAlignedRight()
        self.lblReadyText = QLabelAlignedRight()
        self.lblStateText = QLabelAlignedRight()
        self.lblAxisXText = QLabel()
        self.lblAxisCText = QLabel()
        self.lblAxisZText = QLabel()

        self.lblAxisXDemandPosition = QLabelAlignedRight('-')
        self.lblAxisCDemandPosition = QLabelAlignedRight('-')
        self.lblAxisZDemandPosition = QLabelAlignedRight('-')
        self.lblAxisXEncoderPosition = QLabelAlignedRight('-')
        self.lblAxisCEncoderPosition = QLabelAlignedRight('-')
        self.lblAxisZEncoderPosition = QLabelAlignedRight('-')
        self.lblAxisXContouringError = QLabelAlignedRight('-')
        self.lblAxisCContouringError = QLabelAlignedRight('-')
        self.lblAxisZContouringError = QLabelAlignedRight('-')
        self.lblAxisXTargetPosition = QLabelAlignedRight('-')
        self.lblAxisCTargetPosition = QLabelAlignedRight('-')
        self.lblAxisZTargetPosition = QLabelAlignedRight('-')
        self.lblAxisXDistanceToGo = QLabelAlignedRight('-')
        self.lblAxisCDistanceToGo = QLabelAlignedRight('-')
        self.lblAxisZDistanceToGo = QLabelAlignedRight('-')
        self.chkAxisXReady = QCheckBoxDisabled()
        self.chkAxisCReady = QCheckBoxDisabled()
        self.chkAxisZReady = QCheckBoxDisabled()
        self.lblAxisXStatusByte = QLabelAlignedRight('-')
        self.lblAxisCStatusByte = QLabelAlignedRight('-')
        self.lblAxisZStatusByte = QLabelAlignedRight('-')

        lytAxisInfo = QGridLayout()
        lytAxisInfo.addWidget(self.lblPositionText, 0, 2)
        lytAxisInfo.addWidget(self.lblEncoderText, 0, 3)
        lytAxisInfo.addWidget(self.lblContouringErrorText, 0, 4)
        lytAxisInfo.addWidget(self.lblTargetPositionText, 0, 5)
        lytAxisInfo.addWidget(self.lblRemainingDistanceText, 0, 6)
        lytAxisInfo.addWidget(self.lblReadyText, 0, 7)
        lytAxisInfo.addWidget(self.lblStateText, 0, 8)
        lytAxisInfo.addWidget(self.lblAxisXText, 1, 1)
        lytAxisInfo.addWidget(self.lblAxisCText, 2, 1)
        lytAxisInfo.addWidget(self.lblAxisZText, 3, 1)
        lytAxisInfo.addWidget(self.lblAxisXDemandPosition, 1, 2)
        lytAxisInfo.addWidget(self.lblAxisCDemandPosition, 2, 2)
        lytAxisInfo.addWidget(self.lblAxisZDemandPosition, 3, 2)
        lytAxisInfo.addWidget(self.lblAxisXEncoderPosition, 1, 3)
        lytAxisInfo.addWidget(self.lblAxisCEncoderPosition, 2, 3)
        lytAxisInfo.addWidget(self.lblAxisZEncoderPosition, 3, 3)
        lytAxisInfo.addWidget(self.lblAxisXContouringError, 1, 4)
        lytAxisInfo.addWidget(self.lblAxisCContouringError, 2, 4)
        lytAxisInfo.addWidget(self.lblAxisZContouringError, 3, 4)
        lytAxisInfo.addWidget(self.lblAxisXTargetPosition, 1, 5)
        lytAxisInfo.addWidget(self.lblAxisCTargetPosition, 2, 5)
        lytAxisInfo.addWidget(self.lblAxisZTargetPosition, 3, 5)
        lytAxisInfo.addWidget(self.lblAxisXDistanceToGo, 1, 6)
        lytAxisInfo.addWidget(self.lblAxisCDistanceToGo, 2, 6)
        lytAxisInfo.addWidget(self.lblAxisZDistanceToGo, 3, 6)
        lytAxisInfo.addWidget(self.chkAxisXReady, 1, 7)
        lytAxisInfo.addWidget(self.chkAxisCReady, 2, 7)
        lytAxisInfo.addWidget(self.chkAxisZReady, 3, 7)
        lytAxisInfo.addWidget(self.lblAxisXStatusByte, 1, 8)
        lytAxisInfo.addWidget(self.lblAxisCStatusByte, 2, 8)
        lytAxisInfo.addWidget(self.lblAxisZStatusByte, 3, 8)

        self.setLayout(lytAxisInfo)

    def changeEvent(self, event):
        if event.type() == QEvent.Type.LanguageChange:
            self.setTitle(self.tr('axis-information'))
            self.lblAxisXText.setText(self.tr('axis-x'))
            self.lblAxisCText.setText(self.tr('axis-c'))
            self.lblAxisZText.setText(self.tr('axis-z'))
            self.lblPositionText.setText(self.tr('position'))
            self.lblEncoderText.setText(self.tr('encoder'))
            self.lblContouringErrorText.setText(self.tr('contouring-error'))
            self.lblTargetPositionText.setText(self.tr('target-position'))
            self.lblRemainingDistanceText.setText(self.tr('remaining-distance'))
            self.lblReadyText.setText(self.tr('ready'))
            self.lblStateText.setText(self.tr('state'))
        super().changeEvent(event)
