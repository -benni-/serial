from PySide.QtCore import *
from PySide.QtGui import *
from Util.PinDialog import PinDialog
import datetime
import logging
import time
import os


class Serial():
    def switchLanguage(self, language_location):
        language, location = language_location.split('_')
        if self.customTranslator.load(os.path.join('Language', '{0}_{1}'.format(language, location))):
            QApplication.removeTranslator(self.customTranslator)
            QApplication.installTranslator(self.customTranslator)
            default = language if language != 'zh' else language_location
            self.defaultTranslator.load(
                os.path.join(
                    QLibraryInfo.location(QLibraryInfo.TranslationsPath),
                    'qt_{0}'.format(default)))
            QApplication.removeTranslator(self.defaultTranslator)
            QApplication.installTranslator(self.defaultTranslator)
            self.config.setAndSave('Settings', 'language', language_location)
            self.btnChangeLanguage.setIcon(QIcon(os.path.join('Images', language_location)))

    def displayLogRecord(self, record):
        item = QListWidgetItem()

        if record.levelno >= logging.ERROR:
            item.setBackground(Qt.red)
        elif record.levelno >= logging.WARNING:
            item.setBackground(Qt.yellow)

        message = '{0} - {1}'.format(
            datetime.datetime.fromtimestamp(record.created).strftime('%Y-%m-%d %H:%M:%S'),
            record.msg)
        item.setText(message)

        if self.logWidget.verticalScrollBar().value() == self.logWidget.verticalScrollBar().maximum():
            self.logWidget.addItem(item)
            self.logWidget.scrollToBottom()
        else:
            self.logWidget.addItem(item)

    def logReferenceDriveOffsets(self, offsetX, offsetZ):
        path = os.path.join('Log', 'ReferenceDriveOffsets.log')
        grid = '{0:19} | {1:>3} | {2:>3} |\n'
        if not os.path.isfile(path):
            # print('y reminder das kann einmal am anfang geprüft werden und nicht immer wieder -> funktion setupLogFiles oder manageLogFiles')
            with open(path, 'a', encoding='utf-8') as f:
                f.write(grid.format('Time', 'X', 'Z'))
                f.write('-'*33 + '\n')

        with open(path, 'a', encoding='utf-8') as f:
            f.write(grid.format(
                time.strftime('%Y-%m-%d %H:%M:%S'),
                offsetX,
                offsetZ)
            )

    def loginUser(self):
        if not self.currentUserLevel == self.OPERATOR:
            self.changeUser(self.OPERATOR)
            return
        pinHash = PinDialog().getSha1PinHash()
        if pinHash == self.config.get('Settings', 'administrator_password_hash'):
            self.changeUser(self.ADMINISTRATOR)
        elif pinHash == self.config.get('Settings', 'shift_leader_password_hash'):
            self.changeUser(self.SHIFTLEADER)

    def changeUser(self, userLevel):
        if self.currentUserLevel == userLevel:
            return
        for action in self.grpActions.actions():
            action.setVisible(action.userLevel & userLevel)
        self.actChangeLanguage.setVisible(action.userLevel & userLevel)
        if userLevel == self.ADMINISTRATOR:
            self.lblLogo.setPixmap(QPixmap())
            self.actSwitchUser.setIcon(QIcon(os.path.join('Images', 'logout')))
            self.actSwitchUser.setText(self.tr('logout'))
            self.userLabel = 'user-admin'
        elif userLevel == self.SHIFTLEADER:
            self.lblLogo.setPixmap(QPixmap())
            self.actSwitchUser.setIcon(QIcon(os.path.join('Images', 'logout')))
            self.actSwitchUser.setText(self.tr('logout'))
            self.userLabel = 'user-foreman'
        else:
            self.actSwitchUser.setIcon(QIcon(os.path.join('Images', 'login')))
            self.actSwitchUser.setText(self.tr('login'))
            self.lblLogo.setPixmap(self.pixLogo)
            self.userLabel = 'user-operator'
        self.currentUserLevel = userLevel
        self.lblUser.setText(self.tr(self.userLabel))
        self.timerAutoChangeUserLevel.start(self.usUserIdleAutoLogoutTimeout)
        logging.info('{0} {1}'.format(self.tr('login-as'), self.tr(self.userLabel))) # Anmeldung als reminder

    def LinguistAutoGenerateTranslationsHelper(self):
        self.tr('user-admin')
        self.tr('user-foreman')
        self.tr('user-operator')
