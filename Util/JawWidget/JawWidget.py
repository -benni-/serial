from .JawWidget_UI import JawWidget_UI
from PySide.QtCore import *
from PySide.QtGui import *
import logging


# Every coordinate in the QGraphicsScene matches the coordinate in reality in um
class JawWidget(QGraphicsView, JawWidget_UI):
    def __init__(self, FPDM):
        super().__init__()
        self.Jaws = FPDM.Jaws
        self.setupUI()

    def displayPockets(self, pockets):
        jawPath = QPainterPath()
        jawPath.moveTo(self.Jaws.radiusClampingDisc+self.sizeGrid.x(), self.Jaws.heightFromBottom-self.sizeGrid.y())
        jawPath.lineTo(self.Jaws.radiusClampingDisc+self.sizeGrid.x(), self.Jaws.heightOversize)

        #reminder was ist das?
        if pockets:
            for pocket in pockets:
                jawPath.lineTo(pocket.x(), pocket.top())
                jawPath.lineTo(pocket.x(), pocket.z())

            jawPath.lineTo(self.Jaws.radiusClampingDisc, pockets[-1].z())
        else:
            jawPath.lineTo(self.Jaws.radiusClampingDisc, self.Jaws.heightOversize)

        jawPath.lineTo(self.Jaws.radiusClampingDisc,                   self.Jaws.heightFromBottom-self.sizeGrid.y())
        jawPath.lineTo(self.Jaws.radiusClampingDisc+self.sizeGrid.x(), self.Jaws.heightFromBottom-self.sizeGrid.y())
        self.pathJaw.setPath(jawPath)

    def displayPaths(self, pockets):
        self.clearPaths()

        sizeCircle = 200
        brush = QBrush(Qt.red)
        pen = QPen(Qt.red)

        for pocket in pockets:
            for path in pocket.paths:
                line = QGraphicsLineItem(path.x1(), path.y1(), path.x2(), path.y2())
                line.setPen(pen)
                circle1 = QGraphicsEllipseItem(path.x1()-sizeCircle/2, path.y1()-sizeCircle/2, sizeCircle, sizeCircle)
                circle2 = QGraphicsEllipseItem(path.x2()-sizeCircle/2, path.y2()-sizeCircle/2, sizeCircle, sizeCircle)
                circle1.setBrush(brush)
                circle2.setBrush(brush)

                self.scene.addItem(line)
                self.scene.addItem(circle1)
                self.scene.addItem(circle2)
                self.itemGroupPaths.addToGroup(line)
                self.itemGroupPaths.addToGroup(circle1)
                self.itemGroupPaths.addToGroup(circle2)


    def displayContour(self, pockets):
        pathContour = QPainterPath()
        for pocket in pockets:
            pathContour.moveTo(pocket.x(), pocket.top())
            pathContour.lineTo(pocket.x(), pocket.z())
            pathContour.lineTo(pocket.right(), pocket.z())
        self.pathContour.setPath(pathContour)
        self.pathContour.setVisible(True)

    def clearContour(self):
        self.pathContour.setVisible(False)

    def clearPaths(self):
        [self.scene.removeItem(item) for item in self.itemGroupPaths.childItems()]

    def centerView(self):
        self.fitInView(self.sceneViewItem, Qt.KeepAspectRatio)
        self.setSceneRect(QRect(
            self.sceneViewItem.rect().left(),
            self.sceneViewItem.rect().top(),
            self.sceneViewItem.rect().width(),
            self.sceneViewItem.rect().height()))

    def resizeEvent(self, event):
        self.centerView()
        return super().event(event)
