from PySide.QtCore import *
from PySide.QtGui import *


class JawWidget_UI():
    def setupUI(self):
        self.scene = QGraphicsScene()

        # Jaw item
        self.pathJaw = QGraphicsPathItem()
        self.pathJaw.setPen(QPen(Qt.red))
        self.pathJaw.setBrush(QBrush(Qt.blue))
        self.scene.addItem(self.pathJaw)

        # Item used for scaling
        self.sceneViewItem = QGraphicsRectItem()
        self.sceneViewItem.setVisible(False)
        self.scene.addItem(self.sceneViewItem)

        # Itemgroup for grid
        self.itemGroupGrid = self.scene.createItemGroup([])

        # Limits that show usable area of jaws
        self.lineVerticalLimit = QGraphicsLineItem()
        self.lineHorizontalLimit = QGraphicsLineItem()
        self.scene.addItem(self.lineVerticalLimit)
        self.scene.addItem(self.lineHorizontalLimit)

        # Itemgroup for paths
        self.itemGroupPaths = self.scene.createItemGroup([])

        # Contour path of future jaw
        self.pathContour = QGraphicsPathItem()
        self.pathContour.setPen(QPen(Qt.green))
        self.scene.addItem(self.pathContour)

        self.setScene(self.scene)
        self.scale(-1,-1)
        self.updateUI()

    def updateUI(self):
        self.origin = QPoint(self.Jaws.radiusClampingDisc, self.Jaws.heightFromBottom)
        self.sizeGrid = QPoint(self.Jaws.maxPocketLength + 500, self.Jaws.maxPocketHeight + 500)

        margin = 1000
        self.sceneViewItem.setRect(self.origin.x()-500 - margin, self.origin.y()+500 + margin, self.sizeGrid.x()+500 + 2*margin, -self.sizeGrid.y()-300 - 2*margin)

        self.updateGrid()
        self.updateLineJawLimits()
        self.centerView()

    def updateLineJawLimits(self):
        self.lineVerticalLimit.setLine(
            self.Jaws.radiusClampingDisc,
            self.Jaws.heightFromBottom - self.Jaws.maxPocketHeight,
            self.Jaws.radiusClampingDisc + self.Jaws.maxPocketLength,
            self.Jaws.heightFromBottom - self.Jaws.maxPocketHeight)
        self.lineHorizontalLimit.setLine(
            self.Jaws.radiusClampingDisc + self.Jaws.maxPocketLength,
            self.Jaws.heightFromBottom - self.Jaws.maxPocketHeight,
            self.Jaws.radiusClampingDisc + self.Jaws.maxPocketLength,
            self.Jaws.heightOversize)

    def updateGrid(self):
        [self.scene.removeItem(item) for item in self.itemGroupGrid.childItems()]

        dottedPen = QPen()
        dottedPen.setDashPattern([1, 5])

        gridItems = list()

        self.gridFrame = QGraphicsRectItem(self.origin.x()-1000, self.origin.y()+1000, self.sizeGrid.x()+1000, -self.sizeGrid.y()-1000)
        gridItems.append(self.gridFrame)

        for n in range(-1000, self.sizeGrid.y(), 1000):
            item = self.QGraphicsTextItemFixedSize(str(round(n/1000)))
            item.translate(-item.boundingRect().width(), -item.boundingRect().height()/2)
            item.moveBy(self.origin.x()+self.sizeGrid.x(), self.origin.y()-n)
            line = QGraphicsLineItem(self.origin.x()-1000, self.origin.y()-n, self.origin.x() + self.sizeGrid.x(), self.origin.y()-n)
            line.setPen(dottedPen)
            gridItems.append(item)
            gridItems.append(line)
        for n in range(-1000, self.sizeGrid.x(), 1000):
            item = self.QGraphicsTextItemFixedSize(str(round(n/1000)))
            item.translate(-item.boundingRect().width()/2, 0)
            item.moveBy(self.origin.x()+n, self.origin.y()-self.sizeGrid.y())
            line = QGraphicsLineItem(self.origin.x()+n, self.origin.y()+1000, self.origin.x()+n, self.origin.y() - self.sizeGrid.y())
            line.setPen(dottedPen)
            gridItems.append(item)
            gridItems.append(line)

        [self.scene.addItem(item) for item in gridItems]
        [self.itemGroupGrid.addToGroup(item) for item in gridItems]

    def QGraphicsTextItemFixedSize(self, text):
        size = 30
        item = QGraphicsTextItem(text)
        item.scale(-size,-size)
        return item
