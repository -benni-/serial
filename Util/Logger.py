from PySide.QtCore import *
import logging
import time
import os


class Logger(QObject):
    newRecord = Signal(object)

    def __init__(self):
        super().__init__()

        filename = os.path.join('Log', '{0}.log'.format(time.strftime('%Y-%m-%d')))
        n = 2
        while os.path.isfile(filename):
            filename = os.path.join('Log', '{0} #{1}.log'.format(time.strftime('%Y-%m-%d'), n))
            n += 1

        logging.basicConfig(
            handlers = [logging.FileHandler(filename, encoding='utf-8')],
            level = logging.DEBUG,
            format = '%(asctime)s - %(msg)s',
            datefmt = '%Y-%m-%d %H:%M:%S')

        root = logging.getLogger()
        root.addFilter(self)
        self.records = list()

    def filter(self, record):
        self.records.append(record)
        self.newRecord.emit(record)
        return True
