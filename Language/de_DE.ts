<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>AxisInformationWidget</name>
    <message>
        <location filename="AxisInformationWidget.py" line="139"/>
        <source>axis-information</source>
        <translation>Achsinformationen</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="143"/>
        <source>position</source>
        <translation>Position</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="144"/>
        <source>encoder</source>
        <translation>Encoder</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="145"/>
        <source>contouring-error</source>
        <translation>Schleppfehler</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="146"/>
        <source>target-position</source>
        <translation>Zielposition</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="147"/>
        <source>remaining-distance</source>
        <translation>Restweg</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="148"/>
        <source>ready</source>
        <translation>Bereit</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="149"/>
        <source>state</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="140"/>
        <source>axis-x</source>
        <translation>X-Achse</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="141"/>
        <source>axis-c</source>
        <translation>C-Achse</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="142"/>
        <source>axis-z</source>
        <translation>Z-Achse</translation>
    </message>
</context>
<context>
    <name>Diagnostics</name>
    <message>
        <location filename="Diagnostics.py" line="100"/>
        <source>axis-x</source>
        <translation>X-Achse</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="101"/>
        <source>axis-c</source>
        <translation>C-Achse</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="102"/>
        <source>axis-z</source>
        <translation>Z-Achse</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="103"/>
        <source>byte</source>
        <translation>Byte</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="104"/>
        <source>position-error</source>
        <translation>Positionsfehler</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="105"/>
        <source>home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="106"/>
        <source>ready</source>
        <translation>Bereit</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="107"/>
        <source>referenced</source>
        <translation>Referenziert</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="108"/>
        <source>error-position</source>
        <translation>Fehlerposition</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="109"/>
        <source>error-code</source>
        <translation>Fehlercode</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="110"/>
        <source>firmware-version</source>
        <translation>Firmwareversion</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="112"/>
        <source>parallel-port</source>
        <translation>Parallelport</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="113"/>
        <source>pin</source>
        <translation>Pin</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="114"/>
        <source>state</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="116"/>
        <source>serial-command</source>
        <translation>Serieller Befehl</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="117"/>
        <source>send-command</source>
        <translation>Sende Befehl</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="99"/>
        <source>axis-state</source>
        <translation>Achsenstatus</translation>
    </message>
</context>
<context>
    <name>ExpertMode</name>
    <message>
        <location filename="ExpertMode.py" line="77"/>
        <source>move-axis</source>
        <translation>Achsen verfahren</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="78"/>
        <source>forward</source>
        <translation>Vorwärts</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="79"/>
        <source>backward</source>
        <translation>Rückwärts</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="80"/>
        <source>upward</source>
        <translation>Aufwärts</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="81"/>
        <source>downward</source>
        <translation>Abwärts</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="83"/>
        <source>mode</source>
        <translation>Modus</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="84"/>
        <source>continuous</source>
        <translation>Kontinuierlich</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="85"/>
        <source>single-step</source>
        <translation>Einzelschritt</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="88"/>
        <source>chuck/unchuck jaws</source>
        <translation>Backen spannen/entspannen</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="89"/>
        <source>ready-signal-on/off</source>
        <translation>Bereitschaftssignal an/aus</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="90"/>
        <source>start/stop-spindle</source>
        <translation>Spindel starten/stoppen</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="91"/>
        <source>spindle-rpm</source>
        <translation>Spindeldrehzahl</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="87"/>
        <source>miscellaneous-controls</source>
        <translation>Weitere Steuerung</translation>
    </message>
</context>
<context>
    <name>FPDM</name>
    <message>
        <location filename="FPDM.py" line="76"/>
        <source>press-on-of-wheel-was-not-successful</source>
        <translation>Fehler beim Andrücken des Bauteils</translation>
    </message>
</context>
<context>
    <name>Jaws</name>
    <message>
        <location filename="Jaws.py" line="64"/>
        <source>is-to-small-for-current-jaws</source>
        <translation>ist für die aktuellen Backen zu klein</translation>
    </message>
</context>
<context>
    <name>Machine</name>
    <message>
        <location filename="Machine.py" line="39"/>
        <source>nanotec-reference-drive</source>
        <translation>Nanotec Referenzfahrt</translation>
    </message>
    <message>
        <location filename="Machine.py" line="42"/>
        <source>software-reference-drive</source>
        <translation>Software Referenzfahrt</translation>
    </message>
    <message>
        <location filename="Machine.py" line="63"/>
        <source>stopping-machine</source>
        <translation>Stoppe Maschine</translation>
    </message>
</context>
<context>
    <name>ProcessJaws</name>
    <message>
        <location filename="ProcessJaws.py" line="175"/>
        <source>choose-jaws</source>
        <translation>Backen wählen</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="207"/>
        <source>low-jaws-long-side-to-center</source>
        <translation>Flache Backen - Lange Seite zur Mitte</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="208"/>
        <source>low-jaws-short-side-to-center</source>
        <translation>Flache Backen - Kurze Seite zur Mitte</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="209"/>
        <source>high-jaws-long-side-to-center</source>
        <translation>Hohe Backen - Lange Seite zur Mitte</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="210"/>
        <source>high-jaws-short-side-to-center</source>
        <translation>Hohe Backen - Kurze Seite zur Mitte</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="171"/>
        <source>turning-out-of-jaws-finished</source>
        <translation>Backen ausdrehen beendet</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="167"/>
        <source>error-while-turning-out-jaws</source>
        <translation>Fehler im beim Backen ausdrehen</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="189"/>
        <source>new-jaws-set</source>
        <translation>Neues Backenprofil erstellt</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="67"/>
        <source>is-turning-out-tool-in-machine?</source>
        <translation>Ist das Ausdrehwerkzeug eingebaut?</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="69"/>
        <source>is-press-on-tool-in-machine?</source>
        <translation>Befindet sich der Andrücker im Werkzeughalter?</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="80"/>
        <source>turning-out-of-jaws-started</source>
        <translation>Backen ausdrehen gestartet</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="81"/>
        <source>turning-out-jaws</source>
        <translation>Backen ausdrehen</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="51"/>
        <source>machine-is-not-prepared-for-any-wheel</source>
        <translation>FPDM ist für kein Rad vorbereitet</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="64"/>
        <source>no-wheel-selected</source>
        <translation>Kein Rad ausgewählt</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="43"/>
        <source>error-calculating-paths</source>
        <translation>Fehler beim Berechnen der Pfade</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="193"/>
        <source>wheel-list</source>
        <translation>Bauteilliste</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="194"/>
        <source>preview-of-jaws</source>
        <translation>Backenvorschau</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="196"/>
        <source>controls</source>
        <translation>Steuerung</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="197"/>
        <source>new-jaws</source>
        <translation>Neue Backen</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="198"/>
        <source>reference-drive</source>
        <translation>Referenzfahrt</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="198"/>
        <source>homing-drive</source>
        <translation>Home-Fahrt</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="199"/>
        <source>stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="200"/>
        <source>re-lathe-current-jaw-profile</source>
        <translation>Nachdrehen</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="200"/>
        <source>calculation-only</source>
        <translation>Nur Berechnung</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="201"/>
        <source>start-lathing-process</source>
        <translation>Ausdrehprozess starten</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="197"/>
        <source>current-jaw-profil
will-be-deleted</source>
        <translation>Aktuelles Backenprofil
wird gelöscht</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="201"/>
        <source>paths-shown-above
will-be-lathed</source>
        <translation>Oben dargestellte Wege
werden abgefahren</translation>
    </message>
</context>
<context>
    <name>ProcessParts</name>
    <message>
        <location filename="ProcessParts.py" line="154"/>
        <source>is-current-wheel-already-processed?</source>
        <translation>Ist das aktuelle Rad bereits bearbeitet?</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="37"/>
        <source>machine-is-not-prepared-for-any-wheel</source>
        <translation>Maschine ist für kein Rad vorbereitet</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="167"/>
        <source>automatic-mode-started</source>
        <translation>Automatikmodus gestartet</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="178"/>
        <source>automatic</source>
        <translation>Automatik</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="173"/>
        <source>automatic: waiting-for-starting-signal</source>
        <translation>Automatik: Warte auf Startsignal</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="189"/>
        <source>automatic-mode-has-ended</source>
        <translation>Automatikmodus beendet</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="194"/>
        <source>single-run-started</source>
        <translation>Einzelfahrt gestartet</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="195"/>
        <source>single-run</source>
        <translation>Einzelfahrt</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="203"/>
        <source>single-run-has-ended</source>
        <translation>Einzelfahrt beendet</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="213"/>
        <source>controls</source>
        <translation>Steuerung</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="221"/>
        <source>start-single-run</source>
        <translation>Einzelfahrt starten</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="222"/>
        <source>stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="223"/>
        <source>start-automatic</source>
        <translation>Automatik starten</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="220"/>
        <source>reference-drive</source>
        <translation>Referenzfahrt</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="220"/>
        <source>homing-drive</source>
        <translation>Home-Fahrt</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="216"/>
        <source>increase-part-length</source>
        <translation>Bauteilänge erhöhen</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="215"/>
        <source>offset-part-length</source>
        <translation>Offset Bauteillänge</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="214"/>
        <source>decrease-part-length</source>
        <translation>Bauteilänge verringern</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="217"/>
        <source>decrease-chamfer-size</source>
        <translation>Fase verkleinern</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="218"/>
        <source>offset-chamfer-size</source>
        <translation>Offset Fase</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="219"/>
        <source>increase-chamfer-size</source>
        <translation>Fase vergrößern</translation>
    </message>
</context>
<context>
    <name>Serial</name>
    <message>
        <location filename="Serial.py" line="69"/>
        <source>application</source>
        <translation>Programm</translation>
    </message>
    <message>
        <location filename="Serial.py" line="69"/>
        <source>started</source>
        <translation>gestartet</translation>
    </message>
    <message>
        <location filename="Serial.py" line="75"/>
        <source>application-is-closing</source>
        <translation>Programm wird geschlossen</translation>
    </message>
    <message>
        <location filename="Serial.py" line="76"/>
        <source>closing</source>
        <translation>Beende</translation>
    </message>
    <message>
        <location filename="Util.py" line="99"/>
        <source>user-admin</source>
        <translation>Administrator</translation>
    </message>
    <message>
        <location filename="Util.py" line="100"/>
        <source>user-foreman</source>
        <translation>Schichtführer</translation>
    </message>
    <message>
        <location filename="Util.py" line="101"/>
        <source>user-operator</source>
        <translation>Bediener</translation>
    </message>
    <message>
        <location filename="Serial.py" line="107"/>
        <source>user:</source>
        <translation>Benutzer:</translation>
    </message>
    <message>
        <location filename="Serial.py" line="104"/>
        <source>state:</source>
        <translation>Status:</translation>
    </message>
    <message>
        <location filename="Serial.py" line="106"/>
        <source>wheel:</source>
        <translation>Bauteil:</translation>
    </message>
    <message>
        <location filename="Serial.py" line="108"/>
        <source>version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="Serial.py" line="93"/>
        <source>process-parts</source>
        <translation>Bauteile bearbeiten</translation>
    </message>
    <message>
        <location filename="Serial.py" line="94"/>
        <source>process-jaws</source>
        <translation>Backen ausdrehen</translation>
    </message>
    <message>
        <location filename="Serial.py" line="95"/>
        <source>preferences</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="Serial.py" line="96"/>
        <source>diagnostics</source>
        <translation>Diagnose</translation>
    </message>
    <message>
        <location filename="Serial.py" line="97"/>
        <source>expert-mode</source>
        <translation>Experte</translation>
    </message>
    <message>
        <location filename="Serial.py" line="102"/>
        <source>exit</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="Serial.py" line="88"/>
        <source>toolbar</source>
        <translation>Toolbar</translation>
    </message>
    <message>
        <location filename="Serial.py" line="89"/>
        <source>axis-information</source>
        <translation>Achsinformationen</translation>
    </message>
    <message>
        <location filename="Serial.py" line="90"/>
        <source>axis-speed</source>
        <translation>Achsgeschwindigkeit</translation>
    </message>
    <message>
        <location filename="Serial.py" line="91"/>
        <source>log</source>
        <translation>Protokoll</translation>
    </message>
    <message>
        <location filename="Util.py" line="95"/>
        <source>login-as</source>
        <translation>Anmeldung als</translation>
    </message>
    <message>
        <location filename="Util.py" line="89"/>
        <source>login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="Util.py" line="85"/>
        <source>logout</source>
        <translation>Logout</translation>
    </message>
</context>
<context>
    <name>Thread</name>
    <message>
        <location filename="Thread.py" line="16"/>
        <source>maschine-is-not-referenced</source>
        <translation>Maschine ist nicht referenziert</translation>
    </message>
    <message>
        <location filename="Thread.py" line="22"/>
        <source>maschine-is-not-ready</source>
        <translation>Maschine ist nicht bereit</translation>
    </message>
</context>
</TS>
