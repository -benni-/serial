<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="zh_CN">
<context>
    <name>AxisInformationWidget</name>
    <message>
        <location filename="AxisInformationWidget.py" line="139"/>
        <source>axis-information</source>
        <translation>轴资料</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="143"/>
        <source>position</source>
        <translation>坐标/位置</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="144"/>
        <source>encoder</source>
        <translation>编码器</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="145"/>
        <source>contouring-error</source>
        <translation>轨迹误差/轮廓误差</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="146"/>
        <source>target-position</source>
        <translation>目标位置</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="147"/>
        <source>remaining-distance</source>
        <translation>剩余距离</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="148"/>
        <source>ready</source>
        <translation>就绪</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="149"/>
        <source>state</source>
        <translation>状态</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="140"/>
        <source>axis-x</source>
        <translation>X-轴</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="141"/>
        <source>axis-c</source>
        <translation>C-轴</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="142"/>
        <source>axis-z</source>
        <translation>Z-轴</translation>
    </message>
</context>
<context>
    <name>Diagnostics</name>
    <message>
        <location filename="Diagnostics.py" line="100"/>
        <source>axis-x</source>
        <translation>X-轴</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="101"/>
        <source>axis-c</source>
        <translation>C-轴</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="102"/>
        <source>axis-z</source>
        <translation>Z-轴</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="103"/>
        <source>byte</source>
        <translation>字节</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="104"/>
        <source>position-error</source>
        <translation>位置误差</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="105"/>
        <source>home</source>
        <translation>初始</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="106"/>
        <source>ready</source>
        <translation>就绪</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="107"/>
        <source>referenced</source>
        <translation>零点定位</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="108"/>
        <source>error-position</source>
        <translation>位置错误</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="109"/>
        <source>error-code</source>
        <translation>代码错误</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="110"/>
        <source>firmware-version</source>
        <translation>固件版本</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="112"/>
        <source>parallel-port</source>
        <translation>并行端口</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="113"/>
        <source>pin</source>
        <translation>管脚</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="114"/>
        <source>state</source>
        <translation>状态</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="116"/>
        <source>serial-command</source>
        <translation>串行命令</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="117"/>
        <source>send-command</source>
        <translation>传递 命令</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="99"/>
        <source>axis-state</source>
        <translation>轴状态</translation>
    </message>
</context>
<context>
    <name>ExpertMode</name>
    <message>
        <location filename="ExpertMode.py" line="77"/>
        <source>move-axis</source>
        <translation>移动-轴</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="78"/>
        <source>forward</source>
        <translation>前进</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="79"/>
        <source>backward</source>
        <translation>后退</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="80"/>
        <source>upward</source>
        <translation>向上</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="81"/>
        <source>downward</source>
        <translation>向下</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="83"/>
        <source>mode</source>
        <translation>状态</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="84"/>
        <source>continuous</source>
        <translation>连续，无间断</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="85"/>
        <source>single-step</source>
        <translation>单步</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="88"/>
        <source>chuck/unchuck jaws</source>
        <translation>夹片 夹紧/松开 </translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="89"/>
        <source>ready-signal-on/off</source>
        <translation>就绪信号 开/关</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="90"/>
        <source>start/stop-spindle</source>
        <translation>转轴 开与关</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="91"/>
        <source>spindle-rpm</source>
        <translation>转轴-RPM 每分钟转数 </translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="87"/>
        <source>miscellaneous-controls</source>
        <translation>其它操纵</translation>
    </message>
</context>
<context>
    <name>FPDM</name>
    <message>
        <location filename="FPDM.py" line="76"/>
        <source>press-on-of-wheel-was-not-successful</source>
        <translation>工件按压错误</translation>
    </message>
</context>
<context>
    <name>Jaws</name>
    <message>
        <location filename="Jaws.py" line="64"/>
        <source>is-to-small-for-current-jaws</source>
        <translation>对于现有夹片来说太小了</translation>
    </message>
</context>
<context>
    <name>Machine</name>
    <message>
        <location filename="Machine.py" line="39"/>
        <source>nanotec-reference-drive</source>
        <translation>NANOTEC-零点定位</translation>
    </message>
    <message>
        <location filename="Machine.py" line="42"/>
        <source>software-reference-drive</source>
        <translation>软件零点定位</translation>
    </message>
    <message>
        <location filename="Machine.py" line="63"/>
        <source>stopping-machine</source>
        <translation>停止机械运转</translation>
    </message>
</context>
<context>
    <name>ProcessJaws</name>
    <message>
        <location filename="ProcessJaws.py" line="175"/>
        <source>choose-jaws</source>
        <translation>选择 夹片</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="207"/>
        <source>low-jaws-long-side-to-center</source>
        <translation>矮的长边向中心的夹片</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="208"/>
        <source>low-jaws-short-side-to-center</source>
        <translation>矮的短边向中心夹片</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="209"/>
        <source>high-jaws-long-side-to-center</source>
        <translation>高的长边向中心夹片</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="210"/>
        <source>high-jaws-short-side-to-center</source>
        <translation>高的短边向中心夹片</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="171"/>
        <source>turning-out-of-jaws-finished</source>
        <translation>夹片切削完毕</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="167"/>
        <source>error-while-turning-out-jaws</source>
        <translation>夹片切削误差</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="67"/>
        <source>is-turning-out-tool-in-machine?</source>
        <translation>切削工具已安装？</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="69"/>
        <source>is-press-on-tool-in-machine?</source>
        <translation>按压工具已安装？</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="80"/>
        <source>turning-out-of-jaws-started</source>
        <translation>夹片切削已开始</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="81"/>
        <source>turning-out-jaws</source>
        <translation>夹片切削</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="189"/>
        <source>new-jaws-set</source>
        <translation>新夹片型面完成</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="51"/>
        <source>machine-is-not-prepared-for-any-wheel</source>
        <translation>机器未就绪</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="64"/>
        <source>no-wheel-selected</source>
        <translation>工件还未选择</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="43"/>
        <source>error-calculating-paths</source>
        <translation>运行指标计算错误</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="193"/>
        <source>wheel-list</source>
        <translation>工件目录</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="194"/>
        <source>preview-of-jaws</source>
        <translation>夹片预观</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="196"/>
        <source>controls</source>
        <translation>操控</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="197"/>
        <source>new-jaws</source>
        <translation>新夹片</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="198"/>
        <source>reference-drive</source>
        <translation>零点定位</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="198"/>
        <source>homing-drive</source>
        <translation>回归初始</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="199"/>
        <source>stop</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="200"/>
        <source>re-lathe-current-jaw-profile</source>
        <translation>切削加工</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="200"/>
        <source>calculation-only</source>
        <translation>只计算</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="201"/>
        <source>start-lathing-process</source>
        <translation>开始切削</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="197"/>
        <source>current-jaw-profil
will-be-deleted</source>
        <translation>正在删除现有夹片型面</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="201"/>
        <source>paths-shown-above
will-be-lathed</source>
        <translation>执行以上可见的运行指标</translation>
    </message>
</context>
<context>
    <name>ProcessParts</name>
    <message>
        <location filename="ProcessParts.py" line="154"/>
        <source>is-current-wheel-already-processed?</source>
        <translation>现有的工件是否已完成加工？</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="37"/>
        <source>machine-is-not-prepared-for-any-wheel</source>
        <translation>机器未就绪</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="167"/>
        <source>automatic-mode-started</source>
        <translation>自动模式已开启</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="178"/>
        <source>automatic</source>
        <translation>自动模式</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="173"/>
        <source>automatic: waiting-for-starting-signal</source>
        <translation>自动模式：开始命令等待中</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="189"/>
        <source>automatic-mode-has-ended</source>
        <translation>自动模式结束</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="194"/>
        <source>single-run-started</source>
        <translation>单步模式已开始</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="195"/>
        <source>single-run</source>
        <translation>单步模式</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="203"/>
        <source>single-run-has-ended</source>
        <translation>单步模式已结束</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="213"/>
        <source>controls</source>
        <translation>操控</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="221"/>
        <source>start-single-run</source>
        <translation>开启单步模式</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="222"/>
        <source>stop</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="223"/>
        <source>start-automatic</source>
        <translation>开始自动模式</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="220"/>
        <source>reference-drive</source>
        <translation>零点定位</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="220"/>
        <source>homing-drive</source>
        <translation>回归初始</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="216"/>
        <source>increase-part-length</source>
        <translation>加长工件长度</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="215"/>
        <source>offset-part-length</source>
        <translation>工件长度偏置</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="214"/>
        <source>decrease-part-length</source>
        <translation>缩小工件长度</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="217"/>
        <source>decrease-chamfer-size</source>
        <translation>缩小削角</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="218"/>
        <source>offset-chamfer-size</source>
        <translation>削角偏置</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="219"/>
        <source>increase-chamfer-size</source>
        <translation>削角扩大</translation>
    </message>
</context>
<context>
    <name>Serial</name>
    <message>
        <location filename="Serial.py" line="69"/>
        <source>application</source>
        <translation>软件</translation>
    </message>
    <message>
        <location filename="Serial.py" line="69"/>
        <source>started</source>
        <translation>开启</translation>
    </message>
    <message>
        <location filename="Serial.py" line="75"/>
        <source>application-is-closing</source>
        <translation>软件关闭</translation>
    </message>
    <message>
        <location filename="Serial.py" line="76"/>
        <source>closing</source>
        <translation>结束</translation>
    </message>
    <message>
        <location filename="Util.py" line="99"/>
        <source>user-admin</source>
        <translation>用户管理员</translation>
    </message>
    <message>
        <location filename="Util.py" line="100"/>
        <source>user-foreman</source>
        <translation>领班</translation>
    </message>
    <message>
        <location filename="Util.py" line="101"/>
        <source>user-operator</source>
        <translation>操作者</translation>
    </message>
    <message>
        <location filename="Serial.py" line="107"/>
        <source>user:</source>
        <translation>用户：</translation>
    </message>
    <message>
        <location filename="Serial.py" line="104"/>
        <source>state:</source>
        <translation>状态L：</translation>
    </message>
    <message>
        <location filename="Serial.py" line="106"/>
        <source>wheel:</source>
        <translation>工件：</translation>
    </message>
    <message>
        <location filename="Serial.py" line="108"/>
        <source>version:</source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="Serial.py" line="93"/>
        <source>process-parts</source>
        <translation>工件加工</translation>
    </message>
    <message>
        <location filename="Serial.py" line="94"/>
        <source>process-jaws</source>
        <translation>夹片加工</translation>
    </message>
    <message>
        <location filename="Serial.py" line="95"/>
        <source>preferences</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="Serial.py" line="96"/>
        <source>diagnostics</source>
        <translation>分析</translation>
    </message>
    <message>
        <location filename="Serial.py" line="97"/>
        <source>expert-mode</source>
        <translation>专家模式</translation>
    </message>
    <message>
        <location filename="Serial.py" line="102"/>
        <source>exit</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="Serial.py" line="88"/>
        <source>toolbar</source>
        <translation>工具栏</translation>
    </message>
    <message>
        <location filename="Serial.py" line="89"/>
        <source>axis-information</source>
        <translation>轴资料</translation>
    </message>
    <message>
        <location filename="Serial.py" line="90"/>
        <source>axis-speed</source>
        <translation>轴速度</translation>
    </message>
    <message>
        <location filename="Serial.py" line="91"/>
        <source>log</source>
        <translation>记录表</translation>
    </message>
    <message>
        <location filename="Util.py" line="95"/>
        <source>login-as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Util.py" line="89"/>
        <source>login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Util.py" line="85"/>
        <source>logout</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Thread</name>
    <message>
        <location filename="Thread.py" line="16"/>
        <source>maschine-is-not-referenced</source>
        <translation>机器未设定零点</translation>
    </message>
    <message>
        <location filename="Thread.py" line="22"/>
        <source>maschine-is-not-ready</source>
        <translation>机器未就绪</translation>
    </message>
</context>
</TS>
