<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_MX">
<context>
    <name>AxisInformationWidget</name>
    <message>
        <location filename="AxisInformationWidget.py" line="139"/>
        <source>axis-information</source>
        <translation>Información de ejes</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="143"/>
        <source>position</source>
        <translation>Posición</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="144"/>
        <source>encoder</source>
        <translation>Encoder</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="145"/>
        <source>contouring-error</source>
        <translation>Error de arrastre</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="146"/>
        <source>target-position</source>
        <translation>Posición de destino</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="147"/>
        <source>remaining-distance</source>
        <translation>Distancia restante</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="148"/>
        <source>ready</source>
        <translation>Preparado</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="149"/>
        <source>state</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="140"/>
        <source>axis-x</source>
        <translation>Eje X</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="141"/>
        <source>axis-c</source>
        <translation>Eje C</translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="142"/>
        <source>axis-z</source>
        <translation>Eje Z</translation>
    </message>
</context>
<context>
    <name>Diagnostics</name>
    <message>
        <location filename="Diagnostics.py" line="100"/>
        <source>axis-x</source>
        <translation>Eje X</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="101"/>
        <source>axis-c</source>
        <translation>Eje C</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="102"/>
        <source>axis-z</source>
        <translation>Eje Z</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="103"/>
        <source>byte</source>
        <translation>Byte</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="104"/>
        <source>position-error</source>
        <translation>Error de posición</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="105"/>
        <source>home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="106"/>
        <source>ready</source>
        <translation>Preparado</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="107"/>
        <source>referenced</source>
        <translation>Referenciado</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="108"/>
        <source>error-position</source>
        <translation>Error de Posición</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="109"/>
        <source>error-code</source>
        <translation>Código de error</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="110"/>
        <source>firmware-version</source>
        <translation>Versión de Firmware</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="112"/>
        <source>parallel-port</source>
        <translation>Puerto Paralelo</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="113"/>
        <source>pin</source>
        <translation>Pin</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="114"/>
        <source>state</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="116"/>
        <source>serial-command</source>
        <translation>Comando serial</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="117"/>
        <source>send-command</source>
        <translation>Enviar comando</translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="99"/>
        <source>axis-state</source>
        <translation>Estado de ejes</translation>
    </message>
</context>
<context>
    <name>ExpertMode</name>
    <message>
        <location filename="ExpertMode.py" line="77"/>
        <source>move-axis</source>
        <translation>Mover Ejes</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="78"/>
        <source>forward</source>
        <translation>Hacia adelande</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="79"/>
        <source>backward</source>
        <translation>Hacia Atrás</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="80"/>
        <source>upward</source>
        <translation>Hacia arriba</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="81"/>
        <source>downward</source>
        <translation>Hacia abajo</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="83"/>
        <source>mode</source>
        <translation>Modo</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="84"/>
        <source>continuous</source>
        <translation>Continuo</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="85"/>
        <source>single-step</source>
        <translation>Solo paso</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="88"/>
        <source>chuck/unchuck jaws</source>
        <translation>Tensar / aflojar garras</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="89"/>
        <source>ready-signal-on/off</source>
        <translation>Señal preparado on/off</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="90"/>
        <source>start/stop-spindle</source>
        <translation>Huso start/stop</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="91"/>
        <source>spindle-rpm</source>
        <translation>Huso rpm</translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="87"/>
        <source>miscellaneous-controls</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FPDM</name>
    <message>
        <location filename="FPDM.py" line="76"/>
        <source>press-on-of-wheel-was-not-successful</source>
        <translation>La sujecion de la rueda no fue satisfactoria</translation>
    </message>
</context>
<context>
    <name>Jaws</name>
    <message>
        <location filename="Jaws.py" line="64"/>
        <source>is-to-small-for-current-jaws</source>
        <translation>Es demasiado pequeño para las actuales garras</translation>
    </message>
</context>
<context>
    <name>Machine</name>
    <message>
        <location filename="Machine.py" line="39"/>
        <source>nanotec-reference-drive</source>
        <translation>Nanotec valor de referencia </translation>
    </message>
    <message>
        <location filename="Machine.py" line="42"/>
        <source>software-reference-drive</source>
        <translation>Software valor de referencia</translation>
    </message>
    <message>
        <location filename="Machine.py" line="63"/>
        <source>stopping-machine</source>
        <translation>Parando máquina</translation>
    </message>
</context>
<context>
    <name>ProcessJaws</name>
    <message>
        <location filename="ProcessJaws.py" line="175"/>
        <source>choose-jaws</source>
        <translation>Elejir garras</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="207"/>
        <source>low-jaws-long-side-to-center</source>
        <translation>Parte baja de las Garras lado  largo hacia el centro</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="208"/>
        <source>low-jaws-short-side-to-center</source>
        <translation>Parte baja de las Garras lado corto hacia al centro</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="209"/>
        <source>high-jaws-long-side-to-center</source>
        <translation>Parte alta de las garras lado largo hacia el centro</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="210"/>
        <source>high-jaws-short-side-to-center</source>
        <translation>Parte alta de las garras lado corto hacia el centro</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="171"/>
        <source>turning-out-of-jaws-finished</source>
        <translation>Torneado de las garras terminado</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="167"/>
        <source>error-while-turning-out-jaws</source>
        <translation>Error al tornear las garras</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="67"/>
        <source>is-turning-out-tool-in-machine?</source>
        <translation>¿Esta la herramienta para el torneado en la máquina?</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="69"/>
        <source>is-press-on-tool-in-machine?</source>
        <translation>¿Está la herramienta para presionar en la máquina?</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="80"/>
        <source>turning-out-of-jaws-started</source>
        <translation>El tornado de garras ha comenzado</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="81"/>
        <source>turning-out-jaws</source>
        <translation>Tornado de garras</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="189"/>
        <source>new-jaws-set</source>
        <translation>Nuevo perfil de garras </translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="51"/>
        <source>machine-is-not-prepared-for-any-wheel</source>
        <translation>La máquina no esta preparada para ninguna rueda</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="64"/>
        <source>no-wheel-selected</source>
        <translation>No hay ninguna rueda seleccionada</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="43"/>
        <source>error-calculating-paths</source>
        <translation>Error al calcular el camino</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="193"/>
        <source>wheel-list</source>
        <translation>Lista de componentes</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="194"/>
        <source>preview-of-jaws</source>
        <translation>Vista previa de garras</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="196"/>
        <source>controls</source>
        <translation>Controles</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="197"/>
        <source>new-jaws</source>
        <translation>Nuevas garras</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="198"/>
        <source>reference-drive</source>
        <translation>Referenciar</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="198"/>
        <source>homing-drive</source>
        <translation>Posición Home</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="199"/>
        <source>stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="200"/>
        <source>re-lathe-current-jaw-profile</source>
        <translation>Reajustar garras</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="200"/>
        <source>calculation-only</source>
        <translation>Solo cálculo</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="201"/>
        <source>start-lathing-process</source>
        <translation>Comenzar proceso de tornado</translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="197"/>
        <source>current-jaw-profil
will-be-deleted</source>
        <translation>El Actual perfil de garras será borrada </translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="201"/>
        <source>paths-shown-above
will-be-lathed</source>
        <translation>Los caminos mostrados arriba serán tornados </translation>
    </message>
</context>
<context>
    <name>ProcessParts</name>
    <message>
        <location filename="ProcessParts.py" line="154"/>
        <source>is-current-wheel-already-processed?</source>
        <translation>¿Está la rueda actual ya procesada?</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="37"/>
        <source>machine-is-not-prepared-for-any-wheel</source>
        <translation>La máquina no está preparada para ninguna rueda</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="167"/>
        <source>automatic-mode-started</source>
        <translation>Comenzar modo automático</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="178"/>
        <source>automatic</source>
        <translation>Automático</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="173"/>
        <source>automatic: waiting-for-starting-signal</source>
        <translation>Automático: Esperando la señal start</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="189"/>
        <source>automatic-mode-has-ended</source>
        <translation>El modo automático ha terminado</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="194"/>
        <source>single-run-started</source>
        <translation>Pieza comenzado</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="195"/>
        <source>single-run</source>
        <translation>Solo una pieza</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="203"/>
        <source>single-run-has-ended</source>
        <translation>Pieza terminada</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="213"/>
        <source>controls</source>
        <translation>Controles</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="221"/>
        <source>start-single-run</source>
        <translation>Comenzar una pieza</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="222"/>
        <source>stop</source>
        <translation>Parar</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="223"/>
        <source>start-automatic</source>
        <translation>Comenzar automático</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="220"/>
        <source>reference-drive</source>
        <translation>Referenciar</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="220"/>
        <source>homing-drive</source>
        <translation>Posición Home</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="216"/>
        <source>increase-part-length</source>
        <translation>Incrementar longitud</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="215"/>
        <source>offset-part-length</source>
        <translation>Offset longitud</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="214"/>
        <source>decrease-part-length</source>
        <translation>Disminuir longitud</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="217"/>
        <source>decrease-chamfer-size</source>
        <translation>Disminuir chaflán </translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="218"/>
        <source>offset-chamfer-size</source>
        <translation>Offset chaflán</translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="219"/>
        <source>increase-chamfer-size</source>
        <translation>Incrementar chaflán</translation>
    </message>
</context>
<context>
    <name>Serial</name>
    <message>
        <location filename="Serial.py" line="69"/>
        <source>application</source>
        <translation>Aplicación</translation>
    </message>
    <message>
        <location filename="Serial.py" line="69"/>
        <source>started</source>
        <translation>Iniciado</translation>
    </message>
    <message>
        <location filename="Serial.py" line="75"/>
        <source>application-is-closing</source>
        <translation>Cerrando aplicación</translation>
    </message>
    <message>
        <location filename="Serial.py" line="76"/>
        <source>closing</source>
        <translation>Cerrando</translation>
    </message>
    <message>
        <location filename="Util.py" line="99"/>
        <source>user-admin</source>
        <translation>Administrador</translation>
    </message>
    <message>
        <location filename="Util.py" line="100"/>
        <source>user-foreman</source>
        <translation>Capataz</translation>
    </message>
    <message>
        <location filename="Util.py" line="101"/>
        <source>user-operator</source>
        <translation>Operador</translation>
    </message>
    <message>
        <location filename="Serial.py" line="107"/>
        <source>user:</source>
        <translation>Usuario:</translation>
    </message>
    <message>
        <location filename="Serial.py" line="104"/>
        <source>state:</source>
        <translation>Estado:</translation>
    </message>
    <message>
        <location filename="Serial.py" line="106"/>
        <source>wheel:</source>
        <translation>Componente:</translation>
    </message>
    <message>
        <location filename="Serial.py" line="108"/>
        <source>version:</source>
        <translation>Versión:</translation>
    </message>
    <message>
        <location filename="Serial.py" line="93"/>
        <source>process-parts</source>
        <translation>Proceso del componente </translation>
    </message>
    <message>
        <location filename="Serial.py" line="94"/>
        <source>process-jaws</source>
        <translation>Proceso de garras</translation>
    </message>
    <message>
        <location filename="Serial.py" line="95"/>
        <source>preferences</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <location filename="Serial.py" line="96"/>
        <source>diagnostics</source>
        <translation>Diagnóstico</translation>
    </message>
    <message>
        <location filename="Serial.py" line="97"/>
        <source>expert-mode</source>
        <translation>Modo experto</translation>
    </message>
    <message>
        <location filename="Serial.py" line="102"/>
        <source>exit</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="Serial.py" line="88"/>
        <source>toolbar</source>
        <translation>Barra de herramientas</translation>
    </message>
    <message>
        <location filename="Serial.py" line="89"/>
        <source>axis-information</source>
        <translation>Información de ejes</translation>
    </message>
    <message>
        <location filename="Serial.py" line="90"/>
        <source>axis-speed</source>
        <translation>Velocidad de ejes</translation>
    </message>
    <message>
        <location filename="Serial.py" line="91"/>
        <source>log</source>
        <translation>Protocolo</translation>
    </message>
    <message>
        <location filename="Util.py" line="95"/>
        <source>login-as</source>
        <translation>Iniciar sesión como</translation>
    </message>
    <message>
        <location filename="Util.py" line="89"/>
        <source>login</source>
        <translation>Iniciar sesión</translation>
    </message>
    <message>
        <location filename="Util.py" line="85"/>
        <source>logout</source>
        <translation>Cerrar sesión</translation>
    </message>
</context>
<context>
    <name>Thread</name>
    <message>
        <location filename="Thread.py" line="16"/>
        <source>maschine-is-not-referenced</source>
        <translation>La máquina no está referenciada</translation>
    </message>
    <message>
        <location filename="Thread.py" line="22"/>
        <source>maschine-is-not-ready</source>
        <translation>La máquina no está preparada</translation>
    </message>
</context>
</TS>
