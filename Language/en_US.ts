<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="en_US">
<context>
    <name>AxisInformationWidget</name>
    <message>
        <location filename="AxisInformationWidget.py" line="139"/>
        <source>axis-information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="143"/>
        <source>position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="144"/>
        <source>encoder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="145"/>
        <source>contouring-error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="146"/>
        <source>target-position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="147"/>
        <source>remaining-distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="148"/>
        <source>ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="149"/>
        <source>state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="140"/>
        <source>axis-x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="141"/>
        <source>axis-c</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AxisInformationWidget.py" line="142"/>
        <source>axis-z</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Diagnostics</name>
    <message>
        <location filename="Diagnostics.py" line="100"/>
        <source>axis-x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="101"/>
        <source>axis-c</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="102"/>
        <source>axis-z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="103"/>
        <source>byte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="104"/>
        <source>position-error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="105"/>
        <source>home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="106"/>
        <source>ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="107"/>
        <source>referenced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="108"/>
        <source>error-position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="109"/>
        <source>error-code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="110"/>
        <source>firmware-version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="112"/>
        <source>parallel-port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="113"/>
        <source>pin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="114"/>
        <source>state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="116"/>
        <source>serial-command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="117"/>
        <source>send-command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Diagnostics.py" line="99"/>
        <source>axis-state</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExpertMode</name>
    <message>
        <location filename="ExpertMode.py" line="77"/>
        <source>move-axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="78"/>
        <source>forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="79"/>
        <source>backward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="80"/>
        <source>upward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="81"/>
        <source>downward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="83"/>
        <source>mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="84"/>
        <source>continuous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="85"/>
        <source>single-step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="88"/>
        <source>chuck/unchuck jaws</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="89"/>
        <source>ready-signal-on/off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="90"/>
        <source>start/stop-spindle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="91"/>
        <source>spindle-rpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExpertMode.py" line="87"/>
        <source>miscellaneous-controls</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FPDM</name>
    <message>
        <location filename="FPDM.py" line="76"/>
        <source>press-on-of-wheel-was-not-successful</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Jaws</name>
    <message>
        <location filename="Jaws.py" line="64"/>
        <source>is-to-small-for-current-jaws</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Machine</name>
    <message>
        <location filename="Machine.py" line="39"/>
        <source>nanotec-reference-drive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Machine.py" line="42"/>
        <source>software-reference-drive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Machine.py" line="63"/>
        <source>stopping-machine</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProcessJaws</name>
    <message>
        <location filename="ProcessJaws.py" line="175"/>
        <source>choose-jaws</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="207"/>
        <source>low-jaws-long-side-to-center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="208"/>
        <source>low-jaws-short-side-to-center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="209"/>
        <source>high-jaws-long-side-to-center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="210"/>
        <source>high-jaws-short-side-to-center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="171"/>
        <source>turning-out-of-jaws-finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="167"/>
        <source>error-while-turning-out-jaws</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="67"/>
        <source>is-turning-out-tool-in-machine?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="69"/>
        <source>is-press-on-tool-in-machine?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="80"/>
        <source>turning-out-of-jaws-started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="81"/>
        <source>turning-out-jaws</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="189"/>
        <source>new-jaws-set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="51"/>
        <source>machine-is-not-prepared-for-any-wheel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="64"/>
        <source>no-wheel-selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="43"/>
        <source>error-calculating-paths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="193"/>
        <source>wheel-list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="194"/>
        <source>preview-of-jaws</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="196"/>
        <source>controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="197"/>
        <source>new-jaws</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="198"/>
        <source>reference-drive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="198"/>
        <source>homing-drive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="199"/>
        <source>stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="200"/>
        <source>re-lathe-current-jaw-profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="200"/>
        <source>calculation-only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="201"/>
        <source>start-lathing-process</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="197"/>
        <source>current-jaw-profil
will-be-deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessJaws.py" line="201"/>
        <source>paths-shown-above
will-be-lathed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProcessParts</name>
    <message>
        <location filename="ProcessParts.py" line="154"/>
        <source>is-current-wheel-already-processed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="37"/>
        <source>machine-is-not-prepared-for-any-wheel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="167"/>
        <source>automatic-mode-started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="178"/>
        <source>automatic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="173"/>
        <source>automatic: waiting-for-starting-signal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="189"/>
        <source>automatic-mode-has-ended</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="194"/>
        <source>single-run-started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="195"/>
        <source>single-run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="203"/>
        <source>single-run-has-ended</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="213"/>
        <source>controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="221"/>
        <source>start-single-run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="222"/>
        <source>stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="223"/>
        <source>start-automatic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="220"/>
        <source>reference-drive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="220"/>
        <source>homing-drive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="216"/>
        <source>increase-part-length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="215"/>
        <source>offset-part-length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="214"/>
        <source>decrease-part-length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="217"/>
        <source>decrease-chamfer-size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="218"/>
        <source>offset-chamfer-size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ProcessParts.py" line="219"/>
        <source>increase-chamfer-size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Serial</name>
    <message>
        <location filename="Serial.py" line="69"/>
        <source>application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Serial.py" line="69"/>
        <source>started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Serial.py" line="75"/>
        <source>application-is-closing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Serial.py" line="76"/>
        <source>closing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Util.py" line="99"/>
        <source>user-admin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Util.py" line="100"/>
        <source>user-foreman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Util.py" line="101"/>
        <source>user-operator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Serial.py" line="107"/>
        <source>user:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Serial.py" line="104"/>
        <source>state:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Serial.py" line="106"/>
        <source>wheel:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Serial.py" line="108"/>
        <source>version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Serial.py" line="93"/>
        <source>process-parts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Serial.py" line="94"/>
        <source>process-jaws</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Serial.py" line="95"/>
        <source>preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Serial.py" line="96"/>
        <source>diagnostics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Serial.py" line="97"/>
        <source>expert-mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Serial.py" line="102"/>
        <source>exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Serial.py" line="88"/>
        <source>toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Serial.py" line="89"/>
        <source>axis-information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Serial.py" line="90"/>
        <source>axis-speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Serial.py" line="91"/>
        <source>log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Util.py" line="95"/>
        <source>login-as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Util.py" line="89"/>
        <source>login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Util.py" line="85"/>
        <source>logout</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Thread</name>
    <message>
        <location filename="Thread.py" line="16"/>
        <source>maschine-is-not-referenced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Thread.py" line="22"/>
        <source>maschine-is-not-ready</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
