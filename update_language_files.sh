#!/bin/bash

# Update translation files with new translations in repository and remove obsolete ones
pyside-lupdate -verbose -noobsolete $(git ls-files | grep \.py$) -ts Language/de_DE.ts Language/en_US.ts Language/es_MX.ts Language/zh_CN.ts

# Open Linguist with translations
linguist-qt4 Language/de_DE.ts #Language/en_US.ts Language/es_MX.ts Language/zh_CN.ts

# Compile .ts files to .qm files which PySide uses for translations
lrelease-qt4 Language/de_DE.ts Language/en_US.ts Language/es_MX.ts Language/zh_CN.ts
