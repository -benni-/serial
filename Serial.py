#!/bin/python
from UI.CustomWidgets import *
from FPDM.Util import CustomConfigParser
from Util import Logger
from Util import Util
from FPDM import FPDM
from UI import Serial_UI
import logging
import time
import sys
import os


class Serial(QMainWindow, Serial_UI.MainWindow, Util.Serial):
    def __init__(self):
        super().__init__()

        splash = QSplashScreen(QPixmap(os.path.join('Images', 'BE_Logo_RGB_cut_small')), Qt.WindowStaysOnTopHint)
        splash.show()
        splash.clearMessage()
        time.sleep(1)

        self.title = 'Serial'
        self.version = '0.961'

        self.config = CustomConfigParser.CustomConfigParser(os.path.join('Config', 'Serial.ini'))
        self.usUserIdleAutoLogoutTimeout = self.config.getmicro('Settings', 'user_idle_auto_logout_timeout')
        self.language = self.config.get('Settings', 'language')

        self.customTranslator = QTranslator()
        self.defaultTranslator = QTranslator()

        # Init Objects and create UI
        self.Logger = Logger.Logger()
        self.FPDM = FPDM.FPDM()
        self.setupUI()

        # Connect signals from Logger to logWidget
        self.Logger.newRecord.connect(self.displayLogRecord)

        # Connect signals from FPDM to UI
        self.lblWheel.setText(self.FPDM.getCurrentWheel().name if self.FPDM.getCurrentWheel() else '-')
        self.FPDM.statusChanged.connect(self.lblMachineState.setText)
        self.FPDM.referenceDriveFinished.connect(self.logReferenceDriveOffsets)
        self.FPDM.wheelChanged.connect(self.lblWheel.setText)

        # Connect actions to load corresponding widget
        self.actProcessParts.triggered.connect(lambda: self.centralWidget().setCurrentWidget(self.ProcessParts))
        self.actProcessJaws.triggered.connect(lambda: self.centralWidget().setCurrentWidget(self.ProcessJaws))
        self.actPreferences.triggered.connect(lambda: self.centralWidget().setCurrentWidget(self.Preferences))
        self.actDiagnostics.triggered.connect(lambda: self.centralWidget().setCurrentWidget(self.Diagnostics))
        self.actExpertMode.triggered.connect(lambda: self.centralWidget().setCurrentWidget(self.ExpertMode))
        self.actSwitchUser.triggered.connect(self.loginUser)
        self.actExit.triggered.connect(self.closeApplication)

        # Connect language actions to load language
        self.actGerman.triggered.connect(lambda: self.switchLanguage('de_DE'))
        self.actEnglish.triggered.connect(lambda: self.switchLanguage('en_US'))
        self.actSpanish.triggered.connect(lambda: self.switchLanguage('es_MX'))
        self.actChinese.triggered.connect(lambda: self.switchLanguage('zh_CN'))

        # Timer for automatic user logout
        self.timerAutoChangeUserLevel = QSingleShotTimer(self)
        self.timerAutoChangeUserLevel.timeout.connect(lambda: self.changeUser(self.OPERATOR))

        # Set default screen, load language, login as operator and show application
        self.actProcessParts.trigger()
        self.switchLanguage(self.language)
        logging.info('{0} v{1} {2}'.format(self.tr('application'), self.version, self.tr('started')))
        self.currentUserLevel = 0
        self.changeUser(self.OPERATOR)
        self.showFullScreen()

    def closeApplication(self):
        logging.info(self.tr('application-is-closing'))
        self.FPDM.setStatus(self.tr('closing'))
        self.FPDM.emergencyStop()
        self.close()

    # Start timer for automatically changing user level after timeout
    def event(self, event):
        if event.type() == QEvent.HoverMove and not self.currentUserLevel == self.OPERATOR:
            self.timerAutoChangeUserLevel.start(self.usUserIdleAutoLogoutTimeout)
        return super().event(event)

    def changeEvent(self, event):
        if event.type() == QEvent.Type.LanguageChange:
            self.toolbar.setWindowTitle(self.tr('toolbar'))
            self.dockAxisInfo.setWindowTitle(self.tr('axis-information'))
            self.dockAxisSpeed.setWindowTitle(self.tr('axis-speed'))
            self.dockLog.setWindowTitle(self.tr('log'))

            self.actProcessParts.setText(self.tr('process-parts'))
            self.actProcessJaws.setText(self.tr('process-jaws'))
            self.actPreferences.setText(self.tr('preferences'))
            self.actDiagnostics.setText(self.tr('diagnostics'))
            self.actExpertMode.setText(self.tr('expert-mode'))
            if self.currentUserLevel == self.ADMINISTRATOR or self.currentUserLevel == self.SHIFTLEADER:
                self.actSwitchUser.setText(self.tr('logout'))
            else:
                self.actSwitchUser.setText(self.tr('login'))
            self.actExit.setText(self.tr('exit'))

            self.lblMachineStateText.setText(self.tr('state:'))
            # self.lblMachineState = QLabel() reminder
            self.lblWheelText.setText(self.tr('wheel:'))
            self.lblUserText.setText(self.tr('user:'))
            self.lblVersionText.setText(self.tr('version:'))
            self.lblUser.setText(self.tr(self.userLabel))
        super().changeEvent(event)


def main():
    app = QApplication(sys.argv)
    serial = Serial()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
