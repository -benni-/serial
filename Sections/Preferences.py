from PySide.QtCore import *
from PySide.QtGui import *
from UI import Preferences_UI


class Preferences(QWidget, Preferences_UI.Widget):
    def __init__(self, FPDM):
        super().__init__()
        self.setupUI()
        self.FPDM = FPDM

        self.txtBaudrate.setText(str(self.FPDM.SerialPort.baudrate))
        self.txtTimeout.setText(str(self.FPDM.SerialPort.timeout))
        self.txtDebounceTime.setText(str(self.FPDM.ParallelPort.debounceTime))

        self.txtIDX.setText(str(self.FPDM.AxisX.ID))
        self.txtStepsPerRevolutionX.setText(str(self.FPDM.AxisX.stepAngle))
        self.txtStepModeX.setText(str(self.FPDM.AxisX.stepMode))
        self.txtPitchX.setText(str(self.FPDM.AxisX.pitch))
        self.txtRampTypeX.setText(str(self.FPDM.AxisX.rampType))
        self.txtRampAccelX.setText(str(self.FPDM.AxisX.accelRampHz))
        self.txtRampDecelX.setText(str(self.FPDM.AxisX.decelRampHz))
        self.txtSpeedDefaultX.setText(str(self.FPDM.AxisX.defaultSpeed))
        self.txtReferencePinParallelPortX.setText(str(self.FPDM.AxisX.referencePin))
        self.txtReferencePinNanotecX.setText(str(self.FPDM.AxisX.inputPin))

        self.txtIDC.setText(str(self.FPDM.AxisC.ID))
        self.txtStepsPerRevolutionC.setText(str(self.FPDM.AxisC.stepAngle))
        self.txtStepModeC.setText(str(self.FPDM.AxisC.stepMode))
        self.txtPitchC.setText(str(self.FPDM.AxisC.pitch))
        self.txtRampTypeC.setText(str(self.FPDM.AxisC.rampType))
        self.txtRampAccelC.setText(str(self.FPDM.AxisC.accelRampHz))
        self.txtRampDecelC.setText(str(self.FPDM.AxisC.decelRampHz))
        self.txtRPMDefaultC.setText(str(self.FPDM.AxisC.defaultRPM))

        self.txtIDZ.setText(str(self.FPDM.AxisZ.ID))
        self.txtStepsPerRevolutionZ.setText(str(self.FPDM.AxisZ.stepAngle))
        self.txtStepModeZ.setText(str(self.FPDM.AxisZ.stepMode))
        self.txtPitchZ.setText(str(self.FPDM.AxisZ.pitch))
        self.txtRampTypeZ.setText(str(self.FPDM.AxisZ.rampType))
        self.txtRampAccelZ.setText(str(self.FPDM.AxisZ.accelRampHz))
        self.txtRampDecelZ.setText(str(self.FPDM.AxisZ.decelRampHz))
        self.txtSpeedDefaultZ.setText(str(self.FPDM.AxisZ.defaultSpeed))
        self.txtReferencePinParallelPortZ.setText(str(self.FPDM.AxisZ.referencePin))
        self.txtReferencePinNanotecZ.setText(str(self.FPDM.AxisZ.inputPin))

        for CoordinateSystem in self.FPDM.CoordinateSystems:
            item = QListWidgetItem(CoordinateSystem.name)
            item.setData(Qt.UserRole, CoordinateSystem)
            self.lstCoordinateSystems.addItem(item)

        self.lstCoordinateSystems.itemSelectionChanged.connect(self.loadCoordinateSystemData)

    def loadCoordinateSystemData(self):
        CoordinateSystem = self.lstCoordinateSystems.selectedItems()[0].data(Qt.UserRole)
        self.txtOffsetX.setText(str(CoordinateSystem.offsetX))
        self.txtOffsetZ.setText(str(CoordinateSystem.offsetZ))
