from PySide.QtCore import *
from PySide.QtGui import *
from UI import ProcessParts_UI
import logging
import time


class ProcessParts(QWidget, ProcessParts_UI.Widget):
    def __init__(self, FPDM):
        super().__init__()
        self.setupUI()
        self.FPDM = FPDM

        self.lblOffsetHeight.setText(str(round(self.FPDM.getOffsetHeight() / 1000, 2)))
        self.lblOffsetChamfer.setText(str(round(self.FPDM.getOffsetChamfer() / 1000, 2)))
        self.FPDM.offsetHeightChanged.connect(lambda offset: self.lblOffsetHeight.setText(str(round(offset / 1000, 2))))
        self.FPDM.offsetChamferChanged.connect(lambda offset: self.lblOffsetChamfer.setText(str(round(offset / 1000, 2))))

        self.btnOffsetHeightMinusMinus.clicked.connect(lambda: self.FPDM.changeOffsetHeight(-100))
        self.btnOffsetHeightMinus.clicked.connect(lambda: self.FPDM.changeOffsetHeight(-10))
        self.btnOffsetHeightPlus.clicked.connect(lambda: self.FPDM.changeOffsetHeight(+10))
        self.btnOffsetHeightPlusPlus.clicked.connect(lambda: self.FPDM.changeOffsetHeight(+100))
        self.btnOffsetChamferMinusMinus.clicked.connect(lambda: self.FPDM.changeOffsetChamfer(-100))
        self.btnOffsetChamferMinus.clicked.connect(lambda: self.FPDM.changeOffsetChamfer(-10))
        self.btnOffsetChamferPlus.clicked.connect(lambda: self.FPDM.changeOffsetChamfer(+10))
        self.btnOffsetChamferPlusPlus.clicked.connect(lambda: self.FPDM.changeOffsetChamfer(+100))

        self.btnReferenceDrive.clicked.connect(lambda: self.FPDM.Thread.activateWithNoReferenceDriveRequired(self.FPDM.startHomingOrReferenceDrive))
        self.btnSingleRun.clicked.connect(lambda: self.FPDM.Thread.activate(self.startRunSingle))
        self.btnKillSwitch.clicked.connect(self.FPDM.emergencyStop)
        self.btnAutomatik.clicked.connect(lambda: self.FPDM.Thread.activate(self.startRunAutomaticThread))

    def Ablauf(self):
        Wheel = self.FPDM.getCurrentWheel()
        seatingPocket = self.FPDM.Jaws.getSeatingPocket()
        if not Wheel or not seatingPocket:
            logging.error(self.tr('machine-is-not-prepared-for-any-wheel'))
            return

        startTime = time.time()
        SpeicherePositionFase = self.savePositionChamfer
        SpeichereHoehePlanen = self.savePositionHeight
        OffsetHoehe = self.FPDM.offsetHeight
        OffsetFase = self.FPDM.offsetChamfer
        Verfahre = self.FPDM.moveAxis
        WarteAuf = self.FPDM.waitForAxisReady
        WarteAufZundPruefeAufAndrueckFehler = lambda: self.FPDM.waitForAxisReadyAndCheckForPressOnError(self.FPDM.AxisZ)
        X = self.FPDM.AxisX
        C = self.FPDM.AxisC
        Z = self.FPDM.AxisZ
        BereitschaftssignalAn = self.FPDM.setReady
        BereitschaftssignalAus = self.FPDM.setBusy
        SpindelAn = self.FPDM.AxisC.rotateLeft
        SpindelAus = self.FPDM.AxisC.stopTravelProfile
        BackenOeffnen = self.FPDM.openJaws
        BackenSchliessen = self.FPDM.closeJaws
        Warte = self.FPDM.sleep

        Bauteillaenge       = Wheel.partLength + seatingPocket.z() - Wheel.wheelBackDepth
        Eintrittshoehe      = Wheel.entryHeight + seatingPocket.z() - Wheel.wheelBackDepth
        Nabenradius         = Wheel.hubRadius
        Bohrungsradius      = Wheel.boreRadius
        Fasenlaenge         = Wheel.chamfer

        VerwendePicco            = lambda: self.FPDM.setCoordinateSystem(self.FPDM.CoordinateSystems.Picco)
        VerwendeSchlichterplatte = lambda: self.FPDM.setCoordinateSystem(self.FPDM.CoordinateSystems.Schlichterplatte)
        VerwendeAndruecker       = lambda: self.FPDM.setCoordinateSystem(self.FPDM.CoordinateSystems.Andruecker)

 ###############################################################################
 # Hier beginnt der Ablauf
 ###############################################################################

        ########################################################################
        # Andrücker
        ########################################################################
        VerwendeAndruecker()

        # Sicherheitsposition
        Verfahre(X, 0)
        Verfahre(Z, Bauteillaenge + 10000)
        WarteAuf(Z)
        WarteAuf(X)

        # Andrücken und Backen schließen
        Verfahre(Z, Eintrittshoehe - 5000)
        WarteAufZundPruefeAufAndrueckFehler()
        BackenSchliessen()
        Warte(1)

        # Sicherheitsposition
        Verfahre(Z, Bauteillaenge + 10000)
        WarteAuf(Z)

        ########################################################################
        # Picco
        ########################################################################
        VerwendePicco()

        # Sicherheitsposition
        Verfahre(X, Bohrungsradius)
        WarteAuf(X)

        # Spindel an und in Vorposition fahren
        SpindelAn()
        Verfahre(Z, Bauteillaenge + 1000 - OffsetFase)
        WarteAuf(Z)

        # Fase
        Verfahre(Z, Bauteillaenge - Fasenlaenge - OffsetFase, 10)
        WarteAuf(Z)
        SpeicherePositionFase() # Protokolliert die Encoder-Position mit der die Fase gefertigt wurde
        Warte(1)

        # Sicherheitsposition
        Verfahre(Z, Bauteillaenge + 10000)
        WarteAuf(Z)

        ########################################################################
        # Schlichterplatte
        ########################################################################
        VerwendeSchlichterplatte()

        # Sicherheitsposition
        Verfahre(X, 0)
        WarteAuf(X)

        # Auf gewünschte Höhe fahren
        Verfahre(Z, Bauteillaenge + OffsetHoehe)
        WarteAuf(Z)
        SpeichereHoehePlanen() # Protokolliert die Encoder-Höhe mit der die Nabe geplant wurde

        # Bauteil planen
        Verfahre(X, Nabenradius + 400, 10)
        WarteAuf(X)

        # Spindel aus, Backen auf und in Home-Position
        SpindelAus()
        Verfahre(Z, Bauteillaenge + 10000)
        WarteAuf(Z)
        Z.moveHome()
        X.moveHome()
        WarteAuf(Z)
        WarteAuf(X)
        BackenOeffnen()

 ###############################################################################
 # Hier endet der Ablauf
 ###############################################################################

        return True

    def startRunAutomatic(self):
        # reminder
        reply = QMessageBox.question(self, self.window().title, self.tr('is-current-wheel-already-processed?'), QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)
        if reply == QMessageBox.Yes:
            runFirstPartWithoutStartSignal = False
        elif reply == QMessageBox.No:
            runFirstPartWithoutStartSignal = True
        else:
            return

        self.FPDM.Thread.activate(lambda: self.startRunAutomaticThread(runFirstPartWithoutStartSignal))

    def startRunAutomaticThread(self):#, runFirstPartWithoutStartSignal):
        try:
            self.FPDM.startHomingOrReferenceDrive()
            logging.info(self.tr('automatic-mode-started'))
            self.FPDM.setStatus(self.tr('automatic'))
            #if runFirstPartWithoutStartSignal:
            #    self.Ablauf()
            self.FPDM.setReady()
            while True:
                self.FPDM.setStatus(self.tr('automatic: waiting-for-starting-signal'))
                while self.FPDM.ParallelPort.getPinDebounced(15):
                    self.FPDM.raiseErrorOnHalt()
                    time.sleep(0.001)
                self.FPDM.setBusy()
                self.FPDM.setStatus(self.tr('automatic'))
                if not self.Ablauf():
                    return False
                self.FPDM.setReady()
        except ValueError:
            pass
        except Exception as exception:
            logging.exception(exception)
            self.FPDM.stopAllAxis()
        finally:
            self.FPDM.setBusy()
            logging.info(self.tr('automatic-mode-has-ended'))

    def startRunSingle(self):
        try:
            self.FPDM.startHomingOrReferenceDrive()
            logging.info(self.tr('single-run-started'))
            self.FPDM.setStatus(self.tr('single-run'))
            self.Ablauf()
        except ValueError:
            pass
        except Exception as exception:
            logging.exception(exception)
            self.FPDM.stopAllAxis()
        finally:
            logging.info(self.tr('single-run-has-ended'))

    def savePositionChamfer(self):
        self.lastPosChamfer = (self.FPDM.AxisX.getEncoderRotary(), self.FPDM.AxisZ.getEncoderRotary())

    def savePositionHeight(self):
        self.lastHeight = self.FPDM.AxisZ.getEncoderRotary()

    def changeEvent(self, event):
        if event.type() == QEvent.Type.LanguageChange:
            self.grpControls.setTitle(self.tr('controls'))
            self.lblDecreasePartLengthText.setText(self.tr('decrease-part-length'))
            self.lblOffsetPartLengthText.setText(self.tr('offset-part-length'))
            self.lblIncreasePartLengthText.setText(self.tr('increase-part-length'))
            self.lblDecreaseChamferText.setText(self.tr('decrease-chamfer-size'))
            self.lblOffsetChamferText.setText(self.tr('offset-chamfer-size'))
            self.lblIncreaseChamferText.setText(self.tr('increase-chamfer-size'))
            self.btnReferenceDrive.setText('{0}\n\n{1}'.format(self.tr('reference-drive'), self.tr('homing-drive')))
            self.btnSingleRun.setText(self.tr('start-single-run'))
            self.btnKillSwitch.setText(self.tr('stop'))
            self.btnAutomatik.setText(self.tr('start-automatic'))
        super().changeEvent(event)
