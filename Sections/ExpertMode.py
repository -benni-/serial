from PySide.QtCore import *
from PySide.QtGui import *
from UI import ExpertMode_UI
import logging


class ExpertMode(QWidget, ExpertMode_UI.Widget):
    def __init__(self, FPDM):
        super().__init__()
        self.setupUI()
        self.FPDM = FPDM

        self.btnModeContinuous.clicked.connect(self.switchModeContinuous)
        self.btnMode1.clicked.connect(lambda: self.switchModeFixedDistance(1000))
        self.btnMode01.clicked.connect(lambda: self.switchModeFixedDistance(100))
        self.btnModeSingleStep.clicked.connect(lambda: self.switchModeFixedDistance(None))
        self.btnMoveForward.clicked.connect(self.moveForward)
        self.btnMoveBackward.clicked.connect(self.moveBackward)
        self.btnMoveUp.clicked.connect(self.moveUp)
        self.btnMoveDown.clicked.connect(self.moveDown)
        self.btnModeContinuous.click()

        self.btnToggleJaws.clicked.connect(lambda: self.FPDM.ParallelPort.setPin(9, not self.FPDM.ParallelPort.getPin(9)))
        # reminder self.btnToggleReadySignal.clicked.connect(lambda: self.FPDM.ParallelPort.setPin(14, self.FPDM.ParallelPort.getPin(14)))
        self.btnToggleSpindle.clicked.connect(self.toggleSpindle)

    def toggleSpindle(self):
        if self.FPDM.AxisC.isReady():
            try:
                rpm = int(self.txtSpindleRPM.text())
                self.FPDM.AxisC.rotateLeft(rpm)
            except Exception as exception:
                logging.exception(exception)
        else:
            self.FPDM.AxisC.stopTravelProfile()

    def switchModeContinuous(self):
        self.setAutoRepeatAll(True)
        self.distance = 5000

    def switchModeFixedDistance(self, distance):
        self.setAutoRepeatAll(False)
        self.distance = distance

    def setAutoRepeatAll(self, mode):
        self.btnMoveForward.setAutoRepeat(mode)
        self.btnMoveBackward.setAutoRepeat(mode)
        self.btnMoveUp.setAutoRepeat(mode)
        self.btnMoveDown.setAutoRepeat(mode)

    def moveForward(self):
        if self.distance:
            self.FPDM.AxisX.moveRelative(-self.distance)
        else:
            self.FPDM.AxisX.moveSteps(-1)

    def moveBackward(self):
        if self.distance:
            self.FPDM.AxisX.moveRelative(self.distance)
        else:
            self.FPDM.AxisX.moveSteps(1)

    def moveUp(self):
        if self.distance:
            self.FPDM.AxisZ.moveRelative(self.distance)
        else:
            self.FPDM.AxisZ.moveSteps(1)

    def moveDown(self):
        if self.distance:
            self.FPDM.AxisZ.moveRelative(-self.distance)
        else:
            self.FPDM.AxisZ.moveSteps(-1)

    def changeEvent(self, event):
        if event.type() == QEvent.Type.LanguageChange:
            self.grpMoveAxis.setTitle(self.tr('move-axis'))
            self.btnMoveForward.setText(self.tr('forward'))
            self.btnMoveBackward.setText(self.tr('backward'))
            self.btnMoveUp.setText(self.tr('upward'))
            self.btnMoveDown.setText(self.tr('downward'))

            self.grpMode.setTitle(self.tr('mode'))
            self.btnModeContinuous.setText(self.tr('continuous'))
            self.btnModeSingleStep.setText(self.tr('single-step'))

            self.grpManualControl.setTitle(self.tr('miscellaneous-controls'))
            self.btnToggleJaws.setText(self.tr('chuck/unchuck jaws'))
            self.btnSetReadySignal.setText(self.tr('ready-signal-on/off'))
            self.btnToggleSpindle.setText(self.tr('start/stop-spindle'))
            self.lblSpindleRPMText.setText(self.tr('spindle-rpm'))
        super().changeEvent(event)
