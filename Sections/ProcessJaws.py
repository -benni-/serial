from PySide.QtCore import *
from PySide.QtGui import *
from FPDM.Jaws.Util import Pocket
from UI import ProcessJaws_UI
import logging


class ProcessJaws(QWidget, ProcessJaws_UI.Widget):
    def __init__(self, FPDM):
        super().__init__()
        self.setupUI(FPDM)
        self.FPDM = FPDM

        for wheel in self.FPDM.Wheels:
            item = QListWidgetItem(wheel.name)
            item.setData(Qt.UserRole, wheel)
            self.lstWheels.addItem(item)
        self.lstWheels.setFixedWidth(self.lstWheels.sizeHintForColumn(0)+15)
        self.grpWheelList.setFixedWidth(self.lstWheels.sizeHintForColumn(0)+31)
        self.newWheel = None
        self.newPockets = list()

        self.btnNewJaw.clicked.connect(self.createNewJaw)
        self.btnKillSwitch.clicked.connect(lambda: self.FPDM.emergencyStop())
        self.btnRecalculateProfile.clicked.connect(self.recalculateProfile)
        self.btnStart.clicked.connect(self.startLathing)

        self.btnReferenceDrive.clicked.connect(lambda: self.FPDM.Thread.activateWithNoReferenceDriveRequired(self.FPDM.startHomingOrReferenceDrive))
        self.lstWheels.itemSelectionChanged.connect(self.newWheelSelected)

        self.JawWidget.displayPockets(self.FPDM.Jaws.getPockets())
        self.FPDM.Jaws.pocketsChanged.connect(lambda: self.JawWidget.displayPockets(self.FPDM.Jaws.getPockets()))
        self.FPDM.Jaws.configurationChanged.connect(self.JawWidget.updateUI)

    def newWheelSelected(self):
        if not self.lstWheels.selectedItems():
            return
        self.newWheel = self.lstWheels.selectedItems()[0].data(Qt.UserRole)
        try:
            self.newPockets = self.FPDM.Jaws.calculatePocketsForWheel(self.newWheel)
            self.updateJawWidget()
        except:
            logging.exception(self.tr('error-calculating-paths'))
            self.newWheel = None
            self.newPockets.clear()
            self.JawWidget.clearPaths()
            self.JawWidget.clearContour()

    def recalculateProfile(self):
        if not self.FPDM.getCurrentWheel():
            logging.warning(self.tr('machine-is-not-prepared-for-any-wheel'))
            return
        self.newWheel = self.FPDM.getCurrentWheel()
        self.newPockets = self.FPDM.Jaws.reCalculatePocketsForWheel(self.newWheel)
        self.updateJawWidget()

    def updateJawWidget(self):
        self.JawWidget.displayContour(self.newPockets)
        self.JawWidget.displayPaths(self.newPockets)

    def startLathing(self):
        wheel = self.newWheel
        if not wheel:
            logging.warning(self.tr('no-wheel-selected'))
            return

        if not QMessageBox.question(self, self.window().title, self.tr('is-turning-out-tool-in-machine?'), QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel) == QMessageBox.Yes:
            return
        if not QMessageBox.question(self, self.window().title, self.tr('is-press-on-tool-in-machine?'), QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel) == QMessageBox.No:
            return

        pockets = self.newPockets.copy()

        self.FPDM.setCurrentWheel(None)
        self.newWheel = None
        self.newPockets.clear()
        self.JawWidget.clearPaths()
        self.lstWheels.clearSelection()

        logging.info(self.tr('turning-out-of-jaws-started'))
        self.FPDM.setStatus(self.tr('turning-out-jaws'))
        self.FPDM.Thread.activate(lambda: self.startLathingThread(wheel, pockets))

    def startLathingThread(self, wheel, pockets):
        try:
            Verfahre = self.FPDM.moveAxis
            X = self.FPDM.AxisX
            Z = self.FPDM.AxisZ
            WarteAuf = self.FPDM.waitForAxisReady
            StarteSpindel = self.FPDM.AxisC.rotateLeft
            StoppeSpindel = self.FPDM.AxisC.stopTravelProfile
            BackenOeffnen = self.FPDM.openJaws
            BackenSchliessen = self.FPDM.closeJaws
            Warte = self.FPDM.sleep
            AbstandVorUndNachposition = 200
            Sicherheitshoehe = self.FPDM.Jaws.heightFromBottom + 5000
            VerwendeAusdrehwerkzeug  = lambda: self.FPDM.setCoordinateSystem(self.FPDM.CoordinateSystems.Ausdrehwerkzeug)

 ###############################################################################
 # Hier beginnt der Ausdrehprozess
 ###############################################################################

            VerwendeAusdrehwerkzeug()
            BackenSchliessen()
            Verfahre(X, 0)
            Verfahre(Z, Sicherheitshoehe)
            WarteAuf(Z)
            WarteAuf(X)
            StarteSpindel(500)

            for pocket in pockets:
                Verfahre(X, -pocket.x())
                WarteAuf(X)
                Verfahre(Z, pocket.top() + AbstandVorUndNachposition)
                WarteAuf(Z)

                for path in pocket.paths:
                    Verfahre(X, -path.x1())
                    WarteAuf(X)
                    Verfahre(Z, path.y1(), 5)
                    WarteAuf(Z)

                    if pocket.isSeatingPocket() and path == pocket.paths[-1]:
                        cuttingSpeedSeatingPocket = 10

                        # Lathe with slow cutting speed only until seatingLength is finished
                        Verfahre(X, -pocket.right() - AbstandVorUndNachposition, cuttingSpeedSeatingPocket)
                        WarteAuf(X)

                        # Lathe the rest of the path with full speed
                        Verfahre(X, -path.x2() - AbstandVorUndNachposition, 60)
                        WarteAuf(X)

                        # Freistich
                        Verfahre(X, -pocket.x())
                        WarteAuf(X)

                        Verfahre(Z, path.y1() - 200, 10)
                        WarteAuf(Z)
                        Warte(2)

                        Verfahre(Z, path.y1() + AbstandVorUndNachposition)
                        WarteAuf(Z)
                    else:
                        Verfahre(X, -path.x2() - AbstandVorUndNachposition, 60)
                        WarteAuf(X)

                    self.FPDM.Jaws.addPocket(Pocket.Pocket(path.x1(), path.y1(), seatingPocket = pocket.isSeatingPocket()))

            StoppeSpindel()
            Verfahre(Z, Sicherheitshoehe)
            WarteAuf(Z)
            X.moveHome()
            Z.moveHome()
            WarteAuf(Z)
            WarteAuf(X)
            BackenOeffnen()

 ###############################################################################
 # Hier endet der Ausdrehprozess
 ###############################################################################

            self.FPDM.setCurrentWheel(wheel)
        except ValueError:
            pass
        except:
            logging.exception(self.tr('error-while-turning-out-jaws'))
            self.FPDM.stopAllAxis()
        finally:
            self.JawWidget.clearContour()
            logging.info(self.tr('turning-out-of-jaws-finished'))

    def createNewJaw(self):
        dialog = QInputDialog()
        dialog.setLabelText(self.tr('choose-jaws'))
        lstJawConfigurations = [section for section in self.FPDM.Jaws.cfgConfigurations.sections()]
        lstJawConfigurationsTranslated = [self.tr(section) for section in self.FPDM.Jaws.cfgConfigurations.sections()]
        dialog.setComboBoxItems(lstJawConfigurationsTranslated)
        dialog.setOption(QInputDialog.UseListViewForComboBoxItems)
        if not dialog.exec_():
            return

        index = lstJawConfigurationsTranslated.index(dialog.textValue())
        self.FPDM.Jaws.reset(lstJawConfigurations[index])
        self.lstWheels.clearSelection()
        self.JawWidget.clearPaths()
        self.JawWidget.clearContour()
        self.FPDM.setCurrentWheel(None)
        logging.info('{0}: {1}'.format(self.tr('new-jaws-set'), self.tr(lstJawConfigurations[index])))

    def changeEvent(self, event):
        if event.type() == QEvent.Type.LanguageChange:
            self.grpWheelList.setTitle(self.tr('wheel-list'))
            self.grpJawPreview.setTitle(self.tr('preview-of-jaws'))

            self.grpControls.setTitle(self.tr('controls'))
            self.btnNewJaw.setText('{0}\n\n({1})'.format(self.tr('new-jaws'), self.tr('current-jaw-profil\nwill-be-deleted')))
            self.btnReferenceDrive.setText('{0}\n\n{1}'.format(self.tr('reference-drive'), self.tr('homing-drive')))
            self.btnKillSwitch.setText(self.tr('stop'))
            self.btnRecalculateProfile.setText('{0}\n\n({1})'.format(self.tr('re-lathe-current-jaw-profile'), self.tr('calculation-only')))
            self.btnStart.setText('{0}\n\n({1})'.format(self.tr('start-lathing-process'), self.tr('paths-shown-above\nwill-be-lathed')))
        super().changeEvent(event)

    # This function is never executed. Its sole purpose is that the automatic language file update includes these fields
    def never(self):
        self.tr('low-jaws-long-side-to-center')
        self.tr('low-jaws-short-side-to-center')
        self.tr('high-jaws-long-side-to-center')
        self.tr('high-jaws-short-side-to-center')
