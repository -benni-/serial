from UI.CustomWidgets import *
from UI import Diagnostics_UI
import logging


class Diagnostics(QWidget, Diagnostics_UI.Widget):
    def __init__(self, FPDM):
        super().__init__()
        self.setupUI()
        self.FPDM = FPDM

        self.FPDM.AxisX.currentState.connect(self.receivedAxisXState)
        self.FPDM.AxisC.currentState.connect(self.receivedAxisCState)
        self.FPDM.AxisZ.currentState.connect(self.receivedAxisZState)

        self.btnSendSerialCommand.clicked.connect(self.sendSerialCommand)

        self.timer = QTimer()
        self.timer.timeout.connect(self.updateWidgetInfo)

    def showEvent(self, event):
        self.lblVersionX.setText(self.FPDM.AxisX.getVersion())
        self.lblVersionC.setText(self.FPDM.AxisC.getVersion())
        self.lblVersionZ.setText(self.FPDM.AxisZ.getVersion())
        self.updateWidgetInfo()
        self.timer.start(1000)

    def hideEvent(self, event):
        self.timer.stop()

    def updateWidgetInfo(self):
        self.updateAxisInfo()
        self.updateParallelPortInfo()

    def sendSerialCommand(self):
        try:
            query = '#{0}\r'.format(self.txtCommand.text())
            reply = self.FPDM.SerialPort.querySerialPortRaw(query)
            self.lblAnswer.setText(reply)
        except Exception as exception:
            self.lblAnswer.setText(None)
            logging.exception(exception)

    def updateParallelPortInfo(self):
        try:
            self.chkParallelPortPin2.setChecked(self.FPDM.ParallelPort.getPin(2))
            self.chkParallelPortPin3.setChecked(self.FPDM.ParallelPort.getPin(3))
            self.chkParallelPortPin4.setChecked(self.FPDM.ParallelPort.getPin(4))
            self.chkParallelPortPin5.setChecked(self.FPDM.ParallelPort.getPin(5))
            self.chkParallelPortPin6.setChecked(self.FPDM.ParallelPort.getPin(6))
            self.chkParallelPortPin7.setChecked(self.FPDM.ParallelPort.getPin(7))
            self.chkParallelPortPin8.setChecked(self.FPDM.ParallelPort.getPin(8))
            self.chkParallelPortPin9.setChecked(self.FPDM.ParallelPort.getPin(9))
            self.chkParallelPortPin10.setChecked(self.FPDM.ParallelPort.getPin(10))
            self.chkParallelPortPin11.setChecked(self.FPDM.ParallelPort.getPin(11))
            self.chkParallelPortPin12.setChecked(self.FPDM.ParallelPort.getPin(12))
            self.chkParallelPortPin13.setChecked(self.FPDM.ParallelPort.getPin(13))
            self.chkParallelPortPin15.setChecked(self.FPDM.ParallelPort.getPin(15))
        except Exception as exception:
            logging.exception(exception)

    def receivedAxisXState(self, statusByte):
        self.lblStatusByteX.setText(str(statusByte))
        self.chkPositionErrorX.setChecked(statusByte & 4)
        self.chkHomeX.setChecked(statusByte & 2)
        self.chkReadyX.setChecked(statusByte & 1)

    def receivedAxisCState(self, statusByte):
        self.lblStatusByteC.setText(str(statusByte))
        self.chkPositionErrorC.setChecked(statusByte & 4)
        self.chkHomeC.setChecked(statusByte & 2)
        self.chkReadyC.setChecked(statusByte & 1)

    def receivedAxisZState(self, statusByte):
        self.lblStatusByteZ.setText(str(statusByte))
        self.chkPositionErrorZ.setChecked(statusByte & 4)
        self.chkHomeZ.setChecked(statusByte & 2)
        self.chkReadyZ.setChecked(statusByte & 1)

    def updateAxisInfo(self):
        self.chkIsReferencedX.setChecked(self.FPDM.AxisX.isReferenced())
        self.chkIsReferencedC.setChecked(self.FPDM.AxisC.isReferenced())
        self.chkIsReferencedZ.setChecked(self.FPDM.AxisZ.isReferenced())

        errorAddressX = self.FPDM.AxisX.getErrorAddress()
        errorAddressC = self.FPDM.AxisC.getErrorAddress()
        errorAddressZ = self.FPDM.AxisZ.getErrorAddress()

        self.lblErrorPosX.setText(str(errorAddressX))
        self.lblErrorPosC.setText(str(errorAddressC))
        self.lblErrorPosZ.setText(str(errorAddressZ))

        self.lblErrorX.setText(str(self.FPDM.AxisX.getError(errorAddressX)))
        self.lblErrorC.setText(str(self.FPDM.AxisC.getError(errorAddressC)))
        self.lblErrorZ.setText(str(self.FPDM.AxisZ.getError(errorAddressZ)))

    def changeEvent(self, event):
        if event.type() == QEvent.Type.LanguageChange:
            self.grpAxisState.setTitle(self.tr('axis-state'))
            self.lblAxisXText.setText('<b>{0}</b>'.format(self.tr('axis-x')))
            self.lblAxisCText.setText('<b>{0}</b>'.format(self.tr('axis-c')))
            self.lblAxisZText.setText('<b>{0}</b>'.format(self.tr('axis-z')))
            self.lblByteText.setText(self.tr('byte'))
            self.lblPositionErrorText.setText(self.tr('position-error'))
            self.lblHomeText.setText(self.tr('home'))
            self.lblReadyText.setText(self.tr('ready'))
            self.lblReferencedText.setText(self.tr('referenced'))
            self.lblErrorPositionText.setText(self.tr('error-position'))
            self.lblErrorCodeText.setText(self.tr('error-code'))
            self.lblVersionText.setText(self.tr('firmware-version'))

            self.grpParallelPort.setTitle(self.tr('parallel-port'))
            self.lblPinText.setText('<b>{0}</b>'.format(self.tr('pin')))
            self.lblStateText.setText('<b>{0}</b>'.format(self.tr('state')))

            self.grpSerialCommand.setTitle(self.tr('serial-command'))
            self.btnSendSerialCommand.setText(self.tr('send-command'))
        super().changeEvent(event)
