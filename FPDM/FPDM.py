from PySide.QtCore import *
from .Machine import Machine
from .Util import CustomConfigParser
from .Util import CoordinateSystems
from .Util import Wheel
from .Jaws import Jaws
import logging
import time
import sys
import os


class PressOnException(Exception):
    """Exception is thrown on error during press on"""


class FPDM(Machine.Machine):
    statusChanged = Signal(str)
    offsetHeightChanged = Signal(int)
    offsetChamferChanged = Signal(int)
    wheelChanged = Signal(str)

    def __init__(self):
        super().__init__()
        dirWheels = os.path.join(os.path.join(os.path.dirname(__file__)), 'Wheels')
        self.Wheels = [Wheel.Wheel(CustomConfigParser.CustomConfigParser(os.path.join(dirWheels, wheel), True)) for wheel in sorted(os.listdir(dirWheels))]
        self.config = CustomConfigParser.CustomConfigParser(os.path.join(os.path.dirname(__file__), 'Config', 'FPDM.ini'))
        self.CoordinateSystems = CoordinateSystems.CoordinateSystems()
        self.Jaws = Jaws.Jaws()
        self.Thread.finished.connect(lambda: self.setStatus('Bereit'))
        self.setCurrentWheel(next((wheel for wheel in self.Wheels if wheel.name == self.config.get('Configuration', 'wheel')), None))
        self.offsetHeight = self.config.getmicro('Offsets', 'height')
        self.offsetChamfer = self.config.getmicro('Offsets', 'chamfer')

    def setCurrentWheel(self, wheel):
        if wheel:
            self.config.setAndSave('Configuration', 'wheel', wheel.name)
            self.wheelChanged.emit(wheel.name)
        else:
            self.config.setAndSave('Configuration', 'wheel', '')
            self.wheelChanged.emit('-')
        self.currentWheel = wheel

    def getCurrentWheel(self):
        return self.currentWheel

    def setCoordinateSystem(self, CoordinateSystem):
        self.AxisX.setOffset(CoordinateSystem.offsetX)
        self.AxisZ.setOffset(CoordinateSystem.offsetZ)

    def changeOffsetHeight(self, value):
        self.offsetHeight = self.offsetHeight + value
        self.offsetHeightChanged.emit(self.offsetHeight)
        self.config.setAndSave('Offsets', 'height', str(round(self.offsetHeight / 1000, 2)))

    def changeOffsetChamfer(self, value):
        self.offsetChamfer = self.offsetChamfer + value
        self.offsetChamferChanged.emit(self.offsetChamfer)
        self.config.setAndSave('Offsets', 'chamfer', str(round(self.offsetChamfer / 1000, 2)))

    def getOffsetHeight(self):
        return self.offsetHeight

    def getOffsetChamfer(self):
        return self.offsetChamfer

    def waitForAxisReadyAndCheckForPressOnError(self, Axis):
        while not Axis.isReady():
            self.raiseErrorOnPressOnError()
            self.raiseErrorOnHalt()
            time.sleep(0.01)

    def raiseErrorOnPressOnError(self):
        if not self.ParallelPort.getPinDebounced(11):
            self.stopAllAxis()
            raise PressOnException(self.tr('press-on-of-wheel-was-not-successful'))

    def setReady(self):
        self.ParallelPort.setPin(14, True)

    def setBusy(self):
        self.ParallelPort.setPin(14, False)

    def openJaws(self):
        self.ParallelPort.setPin(9, False)

    def closeJaws(self):
        self.ParallelPort.setPin(9, True)

    def setStatus(self, text):
        self.statusChanged.emit(text)
