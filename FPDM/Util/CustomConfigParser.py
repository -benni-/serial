from configparser import ConfigParser
import io


class CustomConfigParser(ConfigParser):
    def __init__(self, path, addFakeHeader=False):
        super().__init__()
        self.path = path
        if addFakeHeader:
            self.read_file(io.StringIO('[root]\n' + open(path, encoding='utf-8').read()))
        else:
            with open(path, encoding='utf-8') as f:
                self.read_file(f)

    def setAndSave(self, *args):
        self.set(*args)
        self.save()

    def save(self):
        with open(self.path, 'w', encoding='utf-8') as f:
            self.write(f)

    # Return value in um instead of mm
    def getmicro(self, section, option):
        return round(self.getfloat(section, option) * 1000)
