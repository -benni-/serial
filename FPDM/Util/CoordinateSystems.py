from . import CustomConfigParser
import os


class CoordinateSystems(list):
    def __init__(self):
        super().__init__()
        self.config = CustomConfigParser.CustomConfigParser(
            os.path.join(
                os.path.dirname(__file__),
                os.pardir,
                'Config',
                'CoordinateSystems.ini'))

        self.Ausdrehwerkzeug = CoordinateSystem(
            'Ausdrehwerkzeug',
            self.config.getmicro('Ausdrehwerkzeug', 'offset_x'),
            self.config.getmicro('Ausdrehwerkzeug', 'offset_z'))

        self.Picco = CoordinateSystem(
            'Picco',
            self.config.getmicro('Picco', 'offset_x'),
            self.config.getmicro('Picco', 'offset_z'))

        self.Schlichterplatte = CoordinateSystem(
            'Schlichterplatte',
            self.config.getmicro('Schlichterplatte', 'offset_x'),
            self.config.getmicro('Schlichterplatte', 'offset_z'))

        self.Andruecker = CoordinateSystem(
            'Andrücker',
            self.config.getmicro('Andruecker', 'offset_x'),
            self.config.getmicro('Andruecker', 'offset_z'))

        self.append(self.Ausdrehwerkzeug)
        self.append(self.Picco)
        self.append(self.Schlichterplatte)
        self.append(self.Andruecker)

class CoordinateSystem():
    def __init__(self, name, offsetX, offsetZ):
        self.name = name
        self.offsetX = offsetX
        self.offsetZ = offsetZ
