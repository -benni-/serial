import os


class Wheel():
    def __init__(self, config):
        self.name = os.path.basename(config.path)

        self.partLength         = config.getmicro('root', 'Bauteillänge')
        self.entryHeight        = config.getmicro('root', 'Eintrittshöhe')
        self.wheelBackDepth     = config.getmicro('root', 'Radrückentiefe')
        self.clampingHeight     = config.getmicro('root', 'Spannflächenhöhe')
        self.hubRadius          = round(config.getmicro('root', 'Nabe_Durchmesser') / 2)
        self.boreRadius         = round(config.getmicro('root', 'Bohrung_Durchmesser') / 2)
        self.outerRadius        = round(config.getmicro('root', 'Außendurchmesser') / 2)
        self.seatingLength      = config.getmicro('root', 'Auflagefläche')
        self.wheelBackGagePointRadius   = round(config.getmicro('root', 'Gagepoint_RR_Durchmesser') / 2)
        self.wheelBackFlatSurfaceRadius = round(config.getmicro('root', 'Planfläche_RR_Durchmesser') / 2)
        self.chamfer            = config.getmicro('root', 'Fase')
