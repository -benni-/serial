import time


class Part():
    def __init__(self, name, duration, facingHeight, chamferHeight):
        self.name = name
        self.timestamp = time.time()
        self.duration = duration
        self.facingHeight = facingHeight
        self.facingOffset = facingOffset
        self.chamferHeight = chamferHeight
        self.chamferOffset = chamferOffset
