from PySide.QtCore import *
from ..Util import CustomConfigParser
from .StepperMotor import StepperMotor
from .Util import ParallelPort
from .Util import SerialPort
from .Util import Thread
import logging
import time
import glob
import os


class Machine(QObject):
    referenceDriveFinished = Signal(int, int)
    speedFactorChanged = Signal(float)

    def __init__(self):
        super().__init__()
        self.cfgMachine = CustomConfigParser.CustomConfigParser(
            os.path.join(os.path.dirname(__file__), 'Config', 'Machine.ini'))
        self.SerialPort = SerialPort.SerialPort(
            self.selectSerialPort(),
            self.cfgMachine.getint('SerialPort', 'baudrate'),
            self.cfgMachine.getfloat('SerialPort', 'timeout'))
        self.ParallelPort = ParallelPort.ParallelPort(
            self.selectParallelPort(),
            self.cfgMachine.getfloat('ParallelPort', 'debounce_time'))
        self.AxisX = self.connectAxis('AxisX')
        self.AxisC = self.connectAxis('AxisC')
        self.AxisZ = self.connectAxis('AxisZ')
        self.Thread = Thread.Thread(self)
        self.speedFactor = 1

    def startHomingOrReferenceDrive(self):
        try:
            self.AxisC.stopTravelProfile()
            self.openJaws()
            if not self.isReferenced():
                self.setStatus(self.tr('nanotec-reference-drive'))
                self.AxisZ.startReferenceDriveNanotec()
                self.AxisX.startReferenceDriveNanotec()
                self.setStatus(self.tr('software-reference-drive'))
                self.referenceDriveFinished.emit(
                    self.AxisX.startReferenceDriveSoftware(),
                    self.AxisZ.startReferenceDriveSoftware()
                )
                logging.info('reference-drive-finished')
            else:
                self.setStatus('moving-in-home-position')
                self.AxisZ.moveHome()
                self.waitForAxisReady(self.AxisZ)
                self.AxisX.moveHome()
                self.waitForAxisReady(self.AxisX)
        except ValueError:
            pass
        except Exception as exception:
            logging.exception(exception)
            self.stopAllAxis()

    def emergencyStop(self):
        try:
            if self.Thread.isRunning():
                self.setStatus(self.tr('stopping-machine'))
                self.Thread.halt()
                self.Thread.wait()
            self.stopAllAxis()
            logging.info('machine-stopped')
            self.setStatus('ready')
        except Exception as exception:
            logging.exception(exception)

    def getSpeedFactor(self):
        return self.speedFactor

    def changeSpeedFactor(self, value):
        oldSpeedFactor = self.speedFactor
        newSpeedFactor = max(0.01, min(round(self.speedFactor + value, 2), 1))
        if oldSpeedFactor != newSpeedFactor:
            self.AxisX.setSpeedFactor(newSpeedFactor)
            self.AxisZ.setSpeedFactor(newSpeedFactor)
            self.speedFactor = newSpeedFactor
            self.speedFactorChanged.emit(newSpeedFactor)

    def sleep(self, duration):
        start = time.time()
        while time.time() - start <= duration:
            self.raiseErrorOnHalt()
            time.sleep(0.01)

    def raiseErrorOnHalt(self):
        if not self.Thread.mtxHalt.tryLock():
            raise ValueError
        self.Thread.mtxHalt.unlock()

    def moveAxis(self, Axis, position, speed=None):
        Axis.moveAbsolute(position, speed)

    def waitForAxisReady(self, Axis):
        while not Axis.isReady():
            self.raiseErrorOnHalt()
            time.sleep(0.01)

    def stopAllAxis(self):
        self.AxisX.stopTravelProfile()
        self.AxisZ.stopTravelProfile()
        self.AxisC.stopTravelProfile()

    def isReferenced(self):
        return self.AxisX.isReferenced() and self.AxisZ.isReferenced()

    def selectSerialPort(self):
        device = self.cfgMachine.get('SerialPort', 'device')
        if device == 'auto':
            device = next(device for device in glob.glob('/dev/ttyUSB*'))
        return device

    def selectParallelPort(self):
        device = self.cfgMachine.get('ParallelPort', 'device')
        if device == 'auto':
            device = next(device for device in glob.glob('/dev/parport*'))
        return device

    def connectAxis(self, name):
        Axis = StepperMotor.StepperMotor(
            self.SerialPort,
            self.ParallelPort,
            self.cfgMachine.getint(name, 'id'),
            self.cfgMachine.getfloat(name, 'step_angle'),
            self.cfgMachine.getint(name, 'step_mode'),
            self.cfgMachine.getint(name, 'pitch'),
            self.cfgMachine.getint(name, 'ramp_type'),
            self.cfgMachine.getint(name, 'ramp_acceleration'),
            self.cfgMachine.getint(name, 'ramp_deceleration'),
            self.cfgMachine.getint(name, 'speed_default'),
            self.cfgMachine.getint(name, 'RPM_default'),
            self.cfgMachine.getint(name, 'reference_pin_parport'),
            self.cfgMachine.getint(name, 'reference_pin_nanotec'))
        return Axis
