from PySide.QtCore import *
import logging
import serial
import time
import re
import io


class SerialPort():
    def __init__(self, device, baudrate, timeout):
        self.device = device
        self.baudrate = baudrate
        self.timeout = timeout

        ser = serial.Serial(device, baudrate, timeout = 0)
        self.com = io.TextIOWrapper(io.BufferedRWPair(ser, ser), newline='\r')
        self.mtxSerialPort = QMutex()

    def raiseSerialException(self, query, reply):
        raise ValueError('SerialPort - Critical error on query: {0}, reply: {1}'.format(query, reply))

    def querySerialPortWithValidation(self, query):
        for n in range(3):
            reply = self.querySerialPortRaw(query)
            re_query = query[1:].strip()
            re_query = re.escape(re_query)
            pattern = '{0}[ ]?([-+\w]+)\r$'.format(re_query)
            match = re.match(pattern, reply)
            if match:
                return match.group(1)
            else:
                logging.warning('SerialPort Validation Error - Query:{0}, Pattern:{1}, Reply:{2}'.format(query, pattern, reply))
        self.raiseSerialException(query, reply)

    def querySerialPortWithValidationInteger(self, query):
        for n in range(3):
            reply = self.querySerialPortWithValidation(query)
            try:
                return int(reply)
            except ValueError:
                logging.warning('Query: {0}, Reply: {1}'.format(query, reply))
        self.raiseSerialException(query, reply)

    def issueSerialPortWithValidation(self, command):
        for n in range(3):
            reply = self.querySerialPortRaw(command)
            if command[1:] == reply:
                return True
        self.raiseSerialException(command, reply)

    def querySerialPortRaw(self, command):
        self.mtxSerialPort.lock()
        startTime = time.time()
        self.com.write(command)
        reply = None
        try:
            while not reply:
                if time.time() - startTime >= self.timeout:
                    # reminder TimeoutError von python oder eigener? und ein wait einbauen hier?
                    raise ValueError('Command {0} timed out'.format(command))
                self.com.flush()
                reply = self.com.read()
        except ValueError:
            pass
        except UnicodeDecodeError:
            logging.warning('UnicodeDecodeError at query: {0}'.format(command))
        except:
            logging.warning('Unknown error during serial communication')
        self.mtxSerialPort.unlock()
        # logging.warning('{1}\t\t -> {2} - {0:.5f}s'.format(time.time()-startTime, command, reply))
        return reply
