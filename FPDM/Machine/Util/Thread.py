from PySide.QtCore import *
import logging


class Thread(QThread):
    def __init__(self, Machine):
        super().__init__()
        self.Machine = Machine
        self.mtxHalt = QMutex()

    def run(self):
        self.target()

    def activate(self, target):
        if not self.Machine.isReferenced():
            logging.info(self.tr('maschine-is-not-referenced'))
            return
        self.activateWithNoReferenceDriveRequired(target)

    def activateWithNoReferenceDriveRequired(self, target):
        if self.isRunning():
            logging.info(self.tr('maschine-is-not-ready'))
            return
        self.target = target
        self.start()

    def halt(self):
        if self.isRunning():
            if self.mtxHalt.tryLock():
                self.wait()
                self.mtxHalt.unlock()
