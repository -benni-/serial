from PySide.QtCore import *
from . import Nanotec
import time


class StepperMotor(QObject, Nanotec.Nanotec):
    demandPosition = Signal(int)
    encoderPosition = Signal(int)
    targetPosition = Signal(int)
    currentState = Signal(int)

    def __init__(self, SerialPort, ParallelPort, ID, stepAngle, stepMode, pitch, rampType,
            accelRampHz, decelRampHz, defaultSpeed, defaultRPM, referencePin, inputPin):
        super().__init__()

        self.SerialPort = SerialPort
        self.ParallelPort = ParallelPort

        self.ID = ID
        self.stepAngle = stepAngle
        self.stepMode = stepMode
        self.pitch = pitch
        self.rampType = rampType
        self.accelRampHz = accelRampHz
        self.decelRampHz = decelRampHz
        self.defaultSpeed = defaultSpeed
        self.defaultRPM = defaultRPM
        self.referencePin = referencePin
        self.inputPin = inputPin

        self.stepsPerMM =  round(360/stepAngle * stepMode / pitch) if pitch else None
        self.stepsPerRevolution = 200 * stepMode
        self.speedFactor = 1
        self.offset = 0

        self.stopTravelProfile()
        self.setStepMode(stepMode)
        self.setRampType(rampType)
        self.setSendStatusWhenCompleted(False)
        self.setAcceleration(round((3000 / (accelRampHz+11.7)) ** 2))
        self.setDeceleration(round((3000 / (decelRampHz+11.7)) ** 2))
        self.setInput6SelectionCycle()
        # self.resetPositionError()

        self.forwardReferenceSwitchTimer = QTimer()
        self.forwardReferenceSwitchTimer.timeout.connect(self.forwardReferenceSwitch)

        # Timer for continous status updates of axis
        self.axisInformationTimer = QTimer()
        self.axisInformationTimer.timeout.connect(lambda: self.currentState.emit(self.getState()))
        if pitch:
            self.axisInformationTimer.timeout.connect(lambda: self.demandPosition.emit(self.getPosition()))
            self.axisInformationTimer.timeout.connect(lambda: self.encoderPosition.emit(self.getEncoderRotary()))
        self.axisInformationTimer.start(25)

    def setOffset(self, offset):
        self.offset = offset

    def moveHome(self):
        self.setPositionTypeAbsolute()
        self.targetPosition.emit(0)
        self.move(0, self.defaultSpeed)

    def moveAbsolute(self, position, speed=None):
        steps = self.calculateStepsFromUM(position + self.offset)
        self.targetPosition.emit(steps)
        self.setPositionTypeAbsolute()
        self.move(steps, speed)

    def moveRelative(self, distance, speed=None):
        self.moveSteps(self.calculateStepsFromUM(distance), speed)

    def moveStepsWait(self, steps, speed=None):
        self.moveSteps(steps, speed)
        self.waitForAxisReady()

    def moveSteps(self, steps, speed=None):
        self.setDirectionRight()
        self.setPositionTypeRelative()
        self.move(steps, speed)

    def move(self, steps, speed):
        speed = speed if speed else self.defaultSpeed
        self.setSteps(steps)
        self.setMaxFrequency(self.calculateFrequencyFromMMperMin(speed))
        self.startTravelProfile()

    def rotateLeft(self, rpm=None):
        rpm = rpm if rpm else self.defaultRPM
        self.setDirectionLeft()
        self.setPositionTypeRotation()
        self.setMaxFrequency(self.calculateFrequencyFromRPM(rpm))
        self.startTravelProfile()

    def startReferenceDriveNanotec(self):
        self.setDirectionRight()
        self.setPositionTypeExternalReferenceDrive()
        self.setInput6SelectionExternalReferenceSwitch()
        self.setMaxFrequency(self.calculateFrequencyFromMMperMin(100))
        self.forwardReferenceSwitchTimer.start(0)
        self.startTravelProfile()
        self.waitForAxisReady()
        self.forwardReferenceSwitchTimer.stop()
        self.setInput6SelectionCycle()

    def forwardReferenceSwitch(self):
        self.ParallelPort.setPin(self.inputPin, not self.ParallelPort.getPinDebounced(self.referencePin))

    def startReferenceDriveSoftware(self):
        self.setDirectionRight()
        while self.ParallelPort.getPinDebounced(self.referencePin):
            self.moveStepsWait(10)
        while not self.ParallelPort.getPinDebounced(self.referencePin):
            self.moveStepsWait(-1)
            time.sleep(0.3) # reminder Hier könnte man mal testen ob sich das sleep überhaupt auswirkt oder ob man es weglassen kann
        self.resetPositionErrorEncoderZero()
        return self.getEncoderRotary()

    def hasAxisStopped(self):
        startTime = False
        while not self.isReady():
            if self.getEncoderRotary() != self.getEncoderRotary():
                return False
            else:
                startTime = startTime if startTime else time.time()
            if time.time() - startTime > 1:
                return True
        return False

    def waitForAxisReady(self):
        while not self.isReady():
            time.sleep(0.001)
        return True

    def isReady(self):
        return self.getState() & 1

    def getSpeedFactor(self):
        return self.speedFactor

    def setSpeedFactor(self, value):
        self.setMaxFrequency(max(round(self.getMaxFrequency() * value / self.getSpeedFactor()), 1))
        self.speedFactor = value
        if not self.isReady():
            self.startTravelProfile()

    def calculateFrequencyFromMMperMin(self, speed):
        return max(round(self.stepsPerMM * speed * self.getSpeedFactor() / 60), 1)

    def calculateFrequencyFromRPM(self, rpm):
        return max(round(rpm * self.stepsPerRevolution * self.getSpeedFactor() / 60), 1)

    def calculateStepsFromUM(self, position):
        return round(self.stepsPerMM * position / 1000)
