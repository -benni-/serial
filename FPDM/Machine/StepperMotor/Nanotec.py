class Nanotec():
    def setAcceleration(self, acceleration):
        return self.SerialPort.issueSerialPortWithValidation('#{0}b{1}\r'.format(self.ID, acceleration))

    def setDeceleration(self, deceleration):
        return self.SerialPort.issueSerialPortWithValidation('#{0}B{1}\r'.format(self.ID, deceleration))

    def setDirection(self, direction):
        return self.SerialPort.issueSerialPortWithValidation('#{0}d{1}\r'.format(self.ID, direction))

    def setDirectionLeft(self):
        return self.setDirection(0)

    def setDirectionRight(self):
        return self.setDirection(1)

    def getEncoderRotary(self):
        return self.SerialPort.querySerialPortWithValidationInteger('#{0}ZI\r'.format(self.ID))

    def getErrorAddress(self):
        return self.SerialPort.querySerialPortWithValidationInteger('#{0}E\r'.format(self.ID))

    def getError(self, address):
        return self.SerialPort.querySerialPortWithValidationInteger('#{0}Z{1}E\r'.format(self.ID, address))

    def getInput6Selection(self):
        return self.SerialPort.querySerialPortWithValidationInteger('#{0}:port_in_f\r'.format(self.ID))

    def setInput6Selection(self, number):
        return self.SerialPort.issueSerialPortWithValidation('#{0}:port_in_f{1}\r'.format(self.ID, number))

    def setInput6SelectionExternalReferenceSwitch(self):
        return self.setInput6Selection(7)

    def setInput6SelectionCycle(self):
        return self.setInput6Selection(11)

    def getMaxFrequency(self):
        return self.SerialPort.querySerialPortWithValidationInteger('#{0}Zo\r'.format(self.ID))

    def setMaxFrequency(self, frequency):
        return self.SerialPort.issueSerialPortWithValidation('#{0}o{1}\r'.format(self.ID, frequency))

    def getPosition(self):
        return self.SerialPort.querySerialPortWithValidationInteger('#{0}C\r'.format(self.ID))

    def resetPositionError(self):
        return self.SerialPort.issueSerialPortWithValidation('#{0}D\r'.format(self.ID))

    def resetPositionErrorEncoderZero(self):
        return self.SerialPort.issueSerialPortWithValidation('#{0}D0\r'.format(self.ID))

    def setPositionType(self, type):
        return self.SerialPort.issueSerialPortWithValidation('#{0}p{1}\r'.format(self.ID, type))

    def setPositionTypeAbsolute(self):
        return self.setPositionType(2)

    def setPositionTypeExternalReferenceDrive(self):
        return self.setPositionType(4)

    def setPositionTypeRelative(self):
        return self.setPositionType(1)

    def setPositionTypeRotation(self):
        return self.setPositionType(5)

    def setRampType(self, rampType):
        return self.SerialPort.issueSerialPortWithValidation('#{0}:ramp_mode{1}\r'.format(self.ID, rampType))

    def isReferenced(self):
        return self.SerialPort.querySerialPortWithValidationInteger('#{0}:is_referenced\r'.format(self.ID))

    def setSendStatusWhenCompleted(self, flag):
        return self.SerialPort.issueSerialPortWithValidation('#{0}J{1:b}\r'.format(self.ID, flag))

    def getState(self):
        return self.SerialPort.querySerialPortWithValidationInteger('#{0:03}$\r'.format(self.ID))

    def setStepMode(self, stepMode):
        return self.SerialPort.issueSerialPortWithValidation('#{0}g{1}\r'.format(self.ID, stepMode))

    def getSteps(self):
        return self.SerialPort.querySerialPortWithValidationInteger('#{0}Zs\r'.format(self.ID))

    def setSteps(self, steps):
        return self.SerialPort.issueSerialPortWithValidation('#{0}s{1}\r'.format(self.ID, steps))

    def startTravelProfile(self):
        return self.SerialPort.issueSerialPortWithValidation('#{0}A\r'.format(self.ID))

    def stopTravelProfile(self):
        return self.SerialPort.issueSerialPortWithValidation('#{0}S1\r'.format(self.ID))

    def getVersion(self):
        return self.SerialPort.querySerialPortRaw('#{0}Zv\r'.format(self.ID))
