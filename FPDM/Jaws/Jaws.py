from PySide.QtCore import *
from ..Util import CustomConfigParser
from .Util import Pockets
from .Util import Pocket
from .Util import Util
import os


class Jaws(QObject, Util.Util):
    configurationChanged = Signal()
    pocketsChanged = Signal()

    def __init__(self):
        super().__init__()

        self.cfgConfigurations = CustomConfigParser.CustomConfigParser(os.path.join(os.path.dirname(__file__), 'Config', 'Configurations.ini'))
        self.config = CustomConfigParser.CustomConfigParser(os.path.join(os.path.dirname(__file__), 'Config', 'Jaws.ini'))

        self.setConfiguration(self.config.get('Configuration', 'name'))
        self.Pockets = Pockets.Pockets(self)

    def setConfiguration(self, jawConfigurationName):
        self.radiusClampingDisc =   round(self.cfgConfigurations.getmicro(jawConfigurationName, 'clamping_disc_diameter')/2)
        self.heightFromBottom =     self.cfgConfigurations.getmicro(jawConfigurationName, 'height_from_ground')
        self.heightOversizeOffset = self.cfgConfigurations.getmicro(jawConfigurationName, 'height_oversize_offset')
        self.cuttingDepth =         self.cfgConfigurations.getmicro(jawConfigurationName, 'cutting_depth')
        self.maxPocketLength =      self.cfgConfigurations.getmicro(jawConfigurationName, 'max_pocket_length')
        self.maxPocketHeight =      self.cfgConfigurations.getmicro(jawConfigurationName, 'max_pocket_height')
        self.heightOversize = self.heightFromBottom + self.heightOversizeOffset
        self.configurationChanged.emit()

    def reset(self, jawConfigurationName):
        self.setConfiguration(jawConfigurationName)
        self.config.setAndSave('Configuration', 'name', jawConfigurationName)
        self.Pockets.reset()
        self.pocketsChanged.emit()

    def getSeatingPocket(self):
        for pocket in self.Pockets:
            if pocket.isSeatingPocket():
                return pocket

    def getPockets(self):
        return self.Pockets

    def addPocket(self, pocket):
        self.Pockets.add(pocket)
        self.pocketsChanged.emit()

    def reCalculatePocketsForWheel(self, wheel):
        oldSeatingPocket = self.getSeatingPocket()
        offsetX = 100
        offsetZ = -100
        seatingPocket = Pocket.Pocket(oldSeatingPocket.x() + offsetX,
                                      oldSeatingPocket.z() + offsetZ,
                                      oldSeatingPocket.right() + offsetX,
                                      oldSeatingPocket.top() + offsetZ,
                                      True)
        return self.calculatePockets(wheel, seatingPocket)

    def calculatePocketsForWheel(self, wheel):
        # Check if seating length is within boundaries of jaw
        if wheel.outerRadius - wheel.seatingLength < self.radiusClampingDisc:
            raise ValueError('{0} {1}'.format(wheel.name, self.tr('is-to-small-for-current-jaws')))
        return self.calculatePockets(wheel, self.calculateSeatingPocket(wheel))

    def calculatePockets(self, wheel, seatingPocket):
        pockets = list()
        pockets.append(self.calculatePocketAboveSeating(seatingPocket))
        pockets.append(seatingPocket)
        pockets.extend(self.calculatePocketsBelowSeating(seatingPocket, wheel))
        pockets = self.removePocketsThatAreToLow(pockets)

        for pocket in pockets:
            pocket.calculatePaths(self.getPockets(), self.radiusClampingDisc, self.cuttingDepth)

        return pockets
