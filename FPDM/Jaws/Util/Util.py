from PySide.QtCore import *
from . import Pocket


class Util():
    def calculateSeatingPocket(self, wheel):
        seatingPocket = Pocket.Pocket(wheel.outerRadius,
                                      self.heightFromBottom - wheel.clampingHeight,
                                      wheel.outerRadius - wheel.seatingLength,
                                      self.heightFromBottom,
                                      True)

        minimumSeatingDistance = 100
        # Find minimum seating pocket hight from seating length
        minimumHeightFromSeatingLength = seatingPocket.z()
        for pocket in self.getPockets():
            if seatingPocket.right() >= pocket.right():
                minimumHeightFromSeatingLength = pocket.z() - minimumSeatingDistance
                break

        # Find minimum seating pocket hight from clampingHeight
        minimumHeightFromClampingHeight = seatingPocket.z()
        for pocket in self.getPockets():
            if seatingPocket.x() >= pocket.x() + minimumSeatingDistance:
                minimumHeightFromClampingHeight = pocket.top() - seatingPocket.height()
                break

        seatingPocket.moveTo(None, min(minimumHeightFromSeatingLength, minimumHeightFromClampingHeight, seatingPocket.z()))
        return seatingPocket

    def calculatePocketAboveSeating(self, seatingPocket):
        return Pocket.Pocket(
            seatingPocket.x() + 1000,
            seatingPocket.top(),
            seatingPocket.x(),
            self.heightOversize)

    def calculatePocketsBelowSeating(self, seatingPocket, wheel):
        pockets = list()

        bottom = seatingPocket.z() - wheel.wheelBackDepth - 100
        lineGagePointToWheelBottom = QLineF(
            wheel.wheelBackGagePointRadius,
            seatingPocket.z(),
            wheel.wheelBackFlatSurfaceRadius,
            bottom)

        gagePointPocket = Pocket.Pocket(
            seatingPocket.right(),
            seatingPocket.z() - 100,
            wheel.wheelBackGagePointRadius,
            seatingPocket.z())

        lastPocket = seatingPocket

        if not gagePointPocket.right() >= seatingPocket.right():
            currentHeight = gagePointPocket.z()
            pointIntersectionX = round(lineGagePointToWheelBottom.intersect(QLine(0,currentHeight,1,currentHeight))[1].x())
            gagePointPocket.setRight(pointIntersectionX)
            pockets.append(gagePointPocket)
            lastPocket = gagePointPocket
            if gagePointPocket.right() <= self.radiusClampingDisc:
                gagePointPocket.setRight(self.radiusClampingDisc)
                return pockets
        else:
            currentHeight = seatingPocket.z()

        while True:
            currentHeight -= self.cuttingDepth
            pointIntersectionX = round(lineGagePointToWheelBottom.intersect(QLine(0,currentHeight,1,currentHeight))[1].x())
            nextPocket = Pocket.Pocket(
                lastPocket.right(),
                currentHeight,
                pointIntersectionX,
                lastPocket.z())

            pockets.append(nextPocket)

            if nextPocket.z() <= bottom:
                nextPocket.setZ(bottom)
                nextPocket.setRight(self.radiusClampingDisc)
                break
            if nextPocket.right() <= self.radiusClampingDisc:
                nextPocket.setRight(self.radiusClampingDisc)
                pointIntersectionZ = round(lineGagePointToWheelBottom.intersect(QLine(self.radiusClampingDisc,0,self.radiusClampingDisc,1))[1].y())
                nextPocket.setZ(pointIntersectionZ)
                break

            lastPocket = nextPocket
        return pockets

    def removePocketsThatAreToLow(self, pockets):
        for n, pocket in enumerate(pockets):
            if pocket.z() <= self.heightFromBottom - self.maxPocketHeight:
                pocket.setRight(self.radiusClampingDisc)
                pocket.setZ(self.heightFromBottom - self.maxPocketHeight)
                del pockets[n+1:]
                break
        return pockets
