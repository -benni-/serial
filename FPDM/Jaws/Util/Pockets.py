from PySide.QtCore import *
from ...Util import CustomConfigParser
from ..Util import Pocket
import os


class Pockets(list):
    def __init__(self, Jaws):
        super().__init__()
        self.Jaws = Jaws
        self.config = CustomConfigParser.CustomConfigParser(os.path.join(os.path.dirname(__file__), os.pardir, 'Config', 'Pockets.ini'))

        self.set([Pocket.Pocket(self.config.getmicro(section, 'length'), self.config.getmicro(section, 'height'), seatingPocket = section == 'Seating') for section in self.config.sections()])

    def reset(self):
        self.set([])

    def add(self, newPocket):
        if newPocket.isSeatingPocket():
            pockets = [Pocket.Pocket(pocket.x(), pocket.z()) for pocket in self if not pocket.isRedundant(newPocket)]
        else:
            pockets = [Pocket.Pocket(pocket.x(), pocket.z(), seatingPocket=pocket.isSeatingPocket()) for pocket in self if not pocket.isRedundant(newPocket)]
        pockets.append(newPocket)
        self.set(pockets)

    def set(self, pockets):
        pockets.sort(key=lambda pocket: pocket.z(), reverse=True)
        pockets.insert(0, Pocket.Pocket(0, self.Jaws.heightOversize))
        pockets.append(Pocket.Pocket(self.Jaws.radiusClampingDisc, 0))

        self.clear()
        for n in range(1, len(pockets)-1):
            self.append(Pocket.Pocket(pockets[n].x(),
                                      pockets[n].z(),
                                      pockets[n+1].x(),
                                      pockets[n-1].z(),
                                      pockets[n].isSeatingPocket()))

        self.config.clear()
        for n, pocket in enumerate(self):
            if pocket.isSeatingPocket():
                section = 'Seating'
            else:
                section = 'Pocket{0}'.format(n)
            self.config.add_section(section)
            self.config.set(section, 'length', str(pocket.x() / 1000))
            self.config.set(section, 'height', str(pocket.z() / 1000))
        self.config.save()
