from PySide.QtCore import QLine


class Pocket():
    def __init__(self, _x, _z, _right=None, _top=None, seatingPocket=False):
        self._x = _x
        self._z = _z
        self._right = _right
        self._top = _top
        self.paths = list()
        self.seatingPocket = seatingPocket

    def isRedundant(self, pocket):
        return self._x <= pocket.x() and self._z >= pocket.z()

    def setSeatingPocket(self, value):
        self.seatingPocket = value

    def isSeatingPocket(self):
        return self.seatingPocket

    def calculatePaths(self, currentPockets, radiusClampingDisc, cuttingDepth):
        self.paths.clear()

        seatingPocketCuttingDepth = 50
        if self.seatingPocket:
            iteration = [self.z()]
            iteration.extend(range(self.z() + seatingPocketCuttingDepth, self.top(), cuttingDepth))
        else:
            iteration = range(self.z(), self.top(), cuttingDepth)

        for currentHeight in iteration:
            correspondingPocket = self.getCorrespondingPocket(currentHeight, currentPockets)
            if correspondingPocket:
                if self._x <= correspondingPocket.x():
                    break
                else:
                    pathEndPositionX = correspondingPocket.x()
            else:
                pathEndPositionX = radiusClampingDisc
            self.paths.append(QLine(self.x(),
                                    currentHeight,
                                    pathEndPositionX,
                                    currentHeight))
        self.paths.reverse()

    def getCorrespondingPocket(self, height, pockets):
        for pocket in reversed(pockets):
            if pocket.z() <= height < pocket.top():
                return pocket
        return False

    def moveTo(self, _x, _z):
        if _x:
            self._right = _x + self.width()
            self._x = _x
        if _z:
            self._top = _z + self.height()
            self._z = _z

    def setZ(self, _z):
        self._z = _z

    def setRight(self, _right):
        self._right = _right

    def width(self):
        return self._right - self._x

    def height(self):
        return self._top - self._z

    def x(self):
        return self._x

    def z(self):
        return self._z

    def right(self):
        return self._right

    def top(self):
        return self._top
