from Util import AxisInformationWidget
from Util import AxisSpeedWidget
from UI.CustomWidgets import *
from Sections import ProcessParts
from Sections import ProcessJaws
from Sections import Preferences
from Sections import Diagnostics
from Sections import ExpertMode
import os


class MainWindow():
    def setupUI(self):
        self.OPERATOR = 1
        self.SHIFTLEADER = 2
        self.ADMINISTRATOR = 4

        self.setWindowTitle(self.title)
        self.createToolBar()
        self.createAxisInformationWidget()
        self.createAxisSpeedWidget()
        self.createLogWidget()
        self.createStatusBar()

        self.ProcessParts = ProcessParts.ProcessParts(self.FPDM)
        self.ProcessJaws = ProcessJaws.ProcessJaws(self.FPDM)
        self.Preferences = Preferences.Preferences(self.FPDM)
        self.ExpertMode = ExpertMode.ExpertMode(self.FPDM)
        self.Diagnostics = Diagnostics.Diagnostics(self.FPDM)

        centralWidget = QStackedWidget()
        centralWidget.setMinimumHeight(300)
        centralWidget.addWidget(self.ProcessParts)
        centralWidget.addWidget(self.ProcessJaws)
        centralWidget.addWidget(self.ExpertMode)
        centralWidget.addWidget(self.Diagnostics)
        centralWidget.addWidget(self.Preferences)

        self.setCentralWidget(centralWidget)

    def createToolBar(self):
        # Toolbar erstellen
        self.toolbar = QToolBar()
        self.toolbar.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolbar.setIconSize(QSize(104, 52))
        self.toolbar.setMovable(False)
        self.toolbar.setFixedHeight(85)
        # Spacer zwischen den Icons in der Toolsbar
        toolbarSpacerLeft = QWidget()
        toolbarSpacerRight = QWidget()
        toolbarSpacerLeft.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        toolbarSpacerRight.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)

        # Actions for Toolbar
        self.actProcessParts = QActionCheckableWithRights(self, QIcon(os.path.join('Images', 'process-parts')))
        self.actProcessJaws = QActionCheckableWithRights(self, QIcon(os.path.join('Images', 'process-jaws')))
        self.actPreferences = QActionCheckableWithRights(self, QIcon(os.path.join('Images', 'preferences')), self.ADMINISTRATOR)
        self.actDiagnostics = QActionCheckableWithRights(self, QIcon(os.path.join('Images', 'diagnostics')), self.ADMINISTRATOR + self.SHIFTLEADER)
        self.actExpertMode = QActionCheckableWithRights(self, QIcon(os.path.join('Images', 'expert-mode')), self.ADMINISTRATOR + self.SHIFTLEADER)
        self.actSwitchUser = QAction(QIcon(os.path.join('Images', 'login')), '', self)
        self.actExit = QAction(QIcon(os.path.join('Images', 'application-exit')), '', self)

        # ActionGroup erstellen, damit nur ein Knopf zur selben Zeit gedrückt sein kann
        self.grpActions = QActionGroup(self)
        self.grpActions.addAction(self.actProcessParts)
        self.grpActions.addAction(self.actProcessJaws)
        self.grpActions.addAction(self.actPreferences)
        self.grpActions.addAction(self.actDiagnostics)
        self.grpActions.addAction(self.actExpertMode)

        # Logo widget
        self.lblLogo = QLabel()
        self.pixLogo = QPixmap(os.path.join('Images', 'BE_Logo_RGB_cut_smaller'))
        self.lblLogo.setPixmap(self.pixLogo)

        # Actions und Spacer in Toolsbar einfügen
        self.toolbar.addAction(self.actProcessParts)
        self.toolbar.addAction(self.actProcessJaws)
        self.toolbar.addWidget(toolbarSpacerLeft)
        self.toolbar.addWidget(self.lblLogo)
        self.toolbar.addAction(self.actPreferences)
        self.toolbar.addAction(self.actDiagnostics)
        self.toolbar.addAction(self.actExpertMode)
        self.actChangeLanguage = self.toolbar.addWidget(self.createLanguageChangeToolButton())
        self.toolbar.addWidget(toolbarSpacerRight)
        self.toolbar.addAction(self.actSwitchUser)
        self.toolbar.addAction(self.actExit)

        # Set additional attributes for actions and widgets
        for action in self.grpActions.actions():
            self.toolbar.widgetForAction(action).setFixedHeight(81)
        self.btnChangeLanguage.setFixedHeight(81)
        self.actChangeLanguage.userLevel = self.ADMINISTRATOR + self.SHIFTLEADER

        # Add toolbar to window
        self.addToolBar(self.toolbar)

    def createLanguageChangeToolButton(self):
        # Language Actions
        self.actGerman = QAction(QIcon(os.path.join('Images', 'de_DE')), 'Deutsch', self)
        self.actEnglish = QAction(QIcon(os.path.join('Images', 'en_US')), 'English', self)
        self.actSpanish = QAction(QIcon(os.path.join('Images', 'es_MX')), 'Español', self)
        self.actChinese = QAction(QIcon(os.path.join('Images', 'zh_CN')), '中文', self)
        self.actGerman.setIconVisibleInMenu(True)
        self.actEnglish.setIconVisibleInMenu(True)
        self.actSpanish.setIconVisibleInMenu(True)
        self.actChinese.setIconVisibleInMenu(True)

        # Language Menu
        languageMenu = QMenu()
        languageMenu.setIcon(QIcon.fromTheme('preferences-system'))
        languageMenu.addAction(self.actGerman)
        languageMenu.addAction(self.actEnglish)
        languageMenu.addAction(self.actSpanish)
        languageMenu.addAction(self.actChinese)

        # ToolButton for Menu
        self.btnChangeLanguage = QToolButton()
        self.btnChangeLanguage.setPopupMode(QToolButton.InstantPopup)
        self.btnChangeLanguage.setFocusPolicy(Qt.NoFocus)
        self.btnChangeLanguage.setMenu(languageMenu)
        return self.btnChangeLanguage

    def createAxisInformationWidget(self):
        self.dockAxisInfo = QDockWidget()
        self.dockAxisInfo.setTitleBarWidget(QWidget())
        self.dockAxisInfo.setFeatures(QDockWidget.DockWidgetClosable)
        self.dockAxisInfo.setFixedHeight(119)
        self.dockAxisInfo.setWidget(AxisInformationWidget.AxisInformationWidget(self.FPDM))
        self.dockAxisInfo.setContentsMargins(10, 0, 10, -14)
        self.addDockWidget(Qt.TopDockWidgetArea, self.dockAxisInfo)

    def createAxisSpeedWidget(self):
        self.dockAxisSpeed = QDockWidget()
        self.dockAxisSpeed.setFeatures(QDockWidget.AllDockWidgetFeatures)
        self.dockAxisSpeed.setTitleBarWidget(QWidget())
        self.dockAxisSpeed.setWidget(AxisSpeedWidget.AxisSpeedWidget(self.FPDM))
        self.dockAxisSpeed.setFixedHeight(119)
        self.dockAxisSpeed.setContentsMargins(10, 0, 10, -11)
        self.dockAxisSpeed.setVisible(False)
        self.addDockWidget(Qt.TopDockWidgetArea, self.dockAxisSpeed)

    def createLogWidget(self):
        self.logWidget = QListWidgetAlternatingColors()
        self.dockLog = QDockWidget()
        self.dockLog.setFeatures(QDockWidget.NoDockWidgetFeatures)
        self.dockLog.setWidget(self.logWidget)
        self.addDockWidget(Qt.BottomDockWidgetArea, self.dockLog)

    def createStatusBar(self):
        self.lblMachineStateText = QLabel()
        self.lblMachineState = QLabel()
        self.lblWheelText = QLabel()
        self.lblWheel = QLabel()
        self.lblUserText = QLabel()
        self.lblUser = QLabel()
        self.lblVersionText = QLabel()

        self.statusBar().addPermanentWidget(QWidget())
        self.statusBar().addPermanentWidget(self.lblMachineStateText)
        self.statusBar().addPermanentWidget(self.lblMachineState, 1)
        self.statusBar().addPermanentWidget(self.lblWheelText)
        self.statusBar().addPermanentWidget(self.lblWheel)
        self.statusBar().addPermanentWidget(QLabel('|'))
        self.statusBar().addPermanentWidget(self.lblUserText)
        self.statusBar().addPermanentWidget(self.lblUser)
        self.statusBar().addPermanentWidget(QLabel('|'))
        self.statusBar().addPermanentWidget(self.lblVersionText)
        self.statusBar().addPermanentWidget(QLabel(self.version))
        self.statusBar().addPermanentWidget(QWidget())
