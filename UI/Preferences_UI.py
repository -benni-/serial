from UI.CustomWidgets import *


class Widget():
    def setupUI(self):
        tabPreferences = QTabWidget()
        tabPreferences.setDocumentMode(True)
        tabPreferences.addTab(self.grpConnections(), 'Verbindungen')
        tabPreferences.addTab(self.grpAxis(), 'Steuerkarten')
        tabPreferences.addTab(self.grpCoordinateSystems(), 'Koordinatensysteme')

        lytMain = QGridLayout()
        lytMain.addWidget(tabPreferences)
        self.setLayout(lytMain)

    def grpCoordinateSystems(self):
        self.lstCoordinateSystems = QListWidgetAlternatingColors()
        self.txtOffsetX = QLineEditAlignedRight()
        self.txtOffsetZ = QLineEditAlignedRight()

        lytMain = QVBoxLayout()
        lytMain.addWidget(self.lstCoordinateSystems)
        lytMain.addWidget(self.txtOffsetX)
        lytMain.addWidget(self.txtOffsetZ)
        lytMain.addStretch()
        lytMain.addStretch()

        grpCoordinateSystems = QWidget()
        grpCoordinateSystems.setLayout(lytMain)
        return grpCoordinateSystems

    def grpConnections(self):
        lytConnections = QVBoxLayout()
        lytConnections.addLayout(QHBoxSqueezed(self.grpSerialPort()))
        lytConnections.addLayout(QHBoxSqueezed(self.grpParallelPort()))
        lytConnections.addStretch()

        grpConnections = QWidget()
        grpConnections.setLayout(lytConnections)
        return grpConnections

    def grpSerialPort(self):
        self.lstSerialPorts = QComboBox()
        self.txtBaudrate = QLineEdit()
        self.txtTimeout = QLineEdit()

        lytSerialPort = QFormLayout()
        lytSerialPort.addRow('Serial Port', self.lstSerialPorts)
        lytSerialPort.addRow('Baudrate', self.txtBaudrate)
        lytSerialPort.addRow('Timeout', self.txtTimeout)

        grpSerialPort = QGroupBox('Serial Port')
        grpSerialPort.setLayout(lytSerialPort)
        return grpSerialPort

    def grpParallelPort(self):
        self.lstParallelPorts = QComboBox()
        self.txtDebounceTime = QLineEdit()

        lytParallelPort = QFormLayout()
        lytParallelPort.addRow('Parallel Port', self.lstParallelPorts)
        lytParallelPort.addRow('Entprellzeit', self.txtDebounceTime)

        grpParallelPort = QGroupBox('Parallel Port')
        grpParallelPort.setLayout(lytParallelPort)
        return grpParallelPort

    def grpAxis(self):
        lytAxis = QHBoxLayout()
        lytAxis.addWidget(self.grpAxisX())
        lytAxis.addWidget(self.grpAxisC())
        lytAxis.addWidget(self.grpAxisZ())

        grpAxis = QWidget()
        grpAxis.setLayout(lytAxis)
        return grpAxis

    def grpAxisX(self):
        self.txtIDX = QLineEdit()
        self.txtStepsPerRevolutionX = QLineEdit()
        self.txtStepModeX = QLineEdit()
        self.txtPitchX = QLineEdit()
        self.txtRampTypeX = QLineEdit()
        self.txtRampAccelX = QLineEdit()
        self.txtRampDecelX = QLineEdit()
        self.txtSpeedDefaultX = QLineEdit()
        self.txtReferencePinParallelPortX = QLineEdit()
        self.txtReferencePinNanotecX = QLineEdit()

        lytAxisX = QFormLayout()
        lytAxisX.addRow('ID', self.txtIDX)
        lytAxisX.addRow('Schritte pro Umdrehung', self.txtStepsPerRevolutionX)
        lytAxisX.addRow('Schrittmodus', self.txtStepModeX)
        lytAxisX.addRow('Steigung', self.txtPitchX)
        lytAxisX.addRow('Rampentyp', self.txtRampTypeX)
        lytAxisX.addRow('Beschleunigungsrampe', self.txtRampAccelX)
        lytAxisX.addRow('Bremsrampe', self.txtRampDecelX)
        lytAxisX.addRow('Geschwindigkeit', self.txtSpeedDefaultX)
        lytAxisX.addRow('U/min', QLineEditDisabled())
        lytAxisX.addRow('Referenzfahrtpin ParPort', self.txtReferencePinParallelPortX)
        lytAxisX.addRow('Referenzfahrtpin Nanotec', self.txtReferencePinNanotecX)

        grpAxisX = QGroupBox('Motor X-Achse')
        grpAxisX.setLayout(lytAxisX)
        return grpAxisX

    def grpAxisC(self):
        self.txtIDC = QLineEdit()
        self.txtStepsPerRevolutionC = QLineEdit()
        self.txtStepModeC = QLineEdit()
        self.txtPitchC = QLineEdit()
        self.txtRampTypeC = QLineEdit()
        self.txtRampAccelC = QLineEdit()
        self.txtRampDecelC = QLineEdit()
        self.txtRPMDefaultC = QLineEdit()

        lytAxisC = QFormLayout()
        lytAxisC.addRow('ID', self.txtIDC)
        lytAxisC.addRow('Schritte pro Umdrehung', self.txtStepsPerRevolutionC)
        lytAxisC.addRow('Schrittmodus', self.txtStepModeC)
        lytAxisC.addRow('Steigung', self.txtPitchC)
        lytAxisC.addRow('Rampentyp', self.txtRampTypeC)
        lytAxisC.addRow('Beschleunigungsrampe', self.txtRampAccelC)
        lytAxisC.addRow('Bremsrampe', self.txtRampDecelC)
        lytAxisC.addRow('Geschwindigkeit', QLineEditDisabled())
        lytAxisC.addRow('U/min', self.txtRPMDefaultC)
        lytAxisC.addRow('Referenzfahrtpin ParPort', QLineEditDisabled())
        lytAxisC.addRow('Referenzfahrtpin Nanotec', QLineEditDisabled())

        grpAxisC = QGroupBox('Motor C-Achse')
        grpAxisC.setLayout(lytAxisC)
        return grpAxisC

    def grpAxisZ(self):
        self.txtIDZ = QLineEdit()
        self.txtStepsPerRevolutionZ = QLineEdit()
        self.txtStepModeZ = QLineEdit()
        self.txtPitchZ = QLineEdit()
        self.txtRampTypeZ = QLineEdit()
        self.txtRampAccelZ = QLineEdit()
        self.txtRampDecelZ = QLineEdit()
        self.txtSpeedDefaultZ = QLineEdit()
        self.txtReferencePinParallelPortZ = QLineEdit()
        self.txtReferencePinNanotecZ = QLineEdit()

        lytAxisZ = QFormLayout()
        lytAxisZ.addRow('ID', self.txtIDZ)
        lytAxisZ.addRow('Schritte pro Umdrehung', self.txtStepsPerRevolutionZ)
        lytAxisZ.addRow('Schrittmodus', self.txtStepModeZ)
        lytAxisZ.addRow('Steigung', self.txtPitchZ)
        lytAxisZ.addRow('Rampentyp', self.txtRampTypeZ)
        lytAxisZ.addRow('Beschleunigungsrampe', self.txtRampAccelZ)
        lytAxisZ.addRow('Bremsrampe', self.txtRampDecelZ)
        lytAxisZ.addRow('Geschwindigkeit', self.txtSpeedDefaultZ)
        lytAxisZ.addRow('U/min', QLineEditDisabled())
        lytAxisZ.addRow('Referenzfahrtpin ParPort', self.txtReferencePinParallelPortZ)
        lytAxisZ.addRow('Referenzfahrtpin Nanotec', self.txtReferencePinNanotecZ)

        grpAxisZ = QGroupBox('Motor Z-Achse')
        grpAxisZ.setLayout(lytAxisZ)
        return grpAxisZ
