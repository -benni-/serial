from UI.CustomWidgets import *


class Widget():
    def setupUI(self):
        lytControlFPDM = QHBoxLayout()
        lytControlFPDM.addWidget(self.groupMoveAxis())
        lytControlFPDM.addWidget(self.groupManualControl())

        lytMain = QVBoxLayout()
        lytMain.addLayout(lytControlFPDM)
        self.setLayout(lytMain)

    def groupManualControl(self):
        self.btnToggleJaws = QPushButtonExpanding()
        self.btnSetReadySignal = QPushButtonExpanding()
        self.btnToggleSpindle = QPushButtonExpanding()
        self.lblSpindleRPMText = QLabel()
        self.txtSpindleRPM = QLineEditAlignedRight('60')

        lytManualControl = QGridLayout()
        lytManualControl.addWidget(self.btnToggleJaws, 1, 1, 1, 2)
        lytManualControl.addWidget(self.btnSetReadySignal, 2, 1, 1, 2)
        lytManualControl.addWidget(self.btnToggleSpindle, 3, 1, 1, 2)
        lytManualControl.addWidget(self.lblSpindleRPMText, 4, 1)
        lytManualControl.addWidget(self.txtSpindleRPM, 4, 2)

        self.grpManualControl = QGroupBox()
        self.grpManualControl.setLayout(lytManualControl)
        self.grpManualControl.setFixedWidth(400)
        return self.grpManualControl

    def groupMode(self):
        self.btnModeContinuous = QRadioButton()
        self.btnMode1 = QRadioButton('1 mm')
        self.btnMode01 = QRadioButton('0.1 mm')
        self.btnModeSingleStep = QRadioButton()

        lytMode = QVBoxLayout()
        lytMode.addWidget(self.btnModeContinuous)
        lytMode.addWidget(self.btnMode1)
        lytMode.addWidget(self.btnMode01)
        lytMode.addWidget(self.btnModeSingleStep)

        self.grpMode = QGroupBox()
        self.grpMode.setLayout(lytMode)
        return self.grpMode

    def groupMoveAxis(self):
        self.btnMoveForward = QPushButtonExpandingAutoRepeat()
        self.btnMoveBackward = QPushButtonExpandingAutoRepeat()
        self.btnMoveUp = QPushButtonExpandingAutoRepeat()
        self.btnMoveDown = QPushButtonExpandingAutoRepeat()

        lytMoveAxis = QGridLayout()
        lytMoveAxis.addWidget(self.btnMoveForward, 2, 1)
        lytMoveAxis.addWidget(self.btnMoveBackward, 2, 3)
        lytMoveAxis.addWidget(self.groupMode(), 2, 2)
        lytMoveAxis.addWidget(self.btnMoveUp, 1, 2)
        lytMoveAxis.addWidget(self.btnMoveDown, 3, 2)

        self.grpMoveAxis = QGroupBox()
        self.grpMoveAxis.setLayout(lytMoveAxis)
        return self.grpMoveAxis
