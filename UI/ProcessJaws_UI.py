from Util.JawWidget.JawWidget import JawWidget
from UI.CustomWidgets import *


class Widget():
    def setupUI(self, FPDM):
        self.JawWidget = JawWidget(FPDM)

        lytControls = QVBoxLayout()
        lytControls.addWidget(self.groupJawPreview())
        lytControls.addWidget(self.groupButtons())

        lytMain = QHBoxLayout()
        lytMain.addWidget(self.groupWheels())
        lytMain.addLayout(lytControls)
        self.setLayout(lytMain)

    def groupWheels(self):
        self.lstWheels = QListWidgetAlternatingColors()

        lytWheelList = QHBoxLayout()
        lytWheelList.addWidget(self.lstWheels)

        self.grpWheelList = QGroupBox()
        self.grpWheelList.setLayout(lytWheelList)
        return self.grpWheelList

    def groupJawPreview(self):
        lytJawPreviw = QVBoxLayout()
        lytJawPreviw.addWidget(self.JawWidget)

        self.grpJawPreview = QGroupBox()
        self.grpJawPreview.setLayout(lytJawPreviw)
        return self.grpJawPreview

    def groupButtons(self):
        self.btnNewJaw = QPushButtonExpanding()
        self.btnReferenceDrive = QPushButtonExpanding()
        self.btnKillSwitch = QPushButtonExpanding()
        self.btnRecalculateProfile = QPushButtonExpanding()
        self.btnStart = QPushButtonExpanding()

        lytButtons = QGridLayout()
        lytButtons.addWidget(self.btnNewJaw, 1, 1)
        lytButtons.addWidget(self.btnReferenceDrive, 1, 2)
        lytButtons.addWidget(self.btnKillSwitch, 1, 3)
        lytButtons.addWidget(self.btnRecalculateProfile, 1, 4)
        lytButtons.addWidget(self.btnStart, 1, 5)

        self.grpControls = QGroupBox()
        self.grpControls.setLayout(lytButtons)
        return self.grpControls
