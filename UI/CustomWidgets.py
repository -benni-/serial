from PySide.QtCore import *
from PySide.QtGui import *


class QHBoxSqueezed(QHBoxLayout):
    def __init__(self, *widgets):
        super().__init__()
        for widget in widgets:
            self.addWidget(widget)
        self.addStretch()


class QVBoxSqueezed(QVBoxLayout):
    def __init__(self, *widgets):
        super().__init__()
        for widget in widgets:
            self.addWidget(widget)
        self.addStretch()


class QActionCheckableWithRights(QAction):
    def __init__(self, parent, icon, user_level=7):
        super().__init__(parent)
        self.setIcon(icon)
        self.setCheckable(True)
        self.userLevel = user_level


class QPushButtonExpanding(QPushButton):
    def __init__(self, *args):
        super().__init__(*args)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)


class QPushButtonExpandingAutoRepeat(QPushButtonExpanding):
    def __init__(self, *args):
        super().__init__(*args)
        self.setAutoRepeat(True)
        self.setAutoRepeatDelay(0)
        self.setAutoRepeatInterval(100)


class QCheckBoxDisabled(QCheckBox):
    def __init__(self):
        super().__init__()
        self.setDisabled(True)
        self.setLayoutDirection(Qt.RightToLeft)


class QLabelCentered(QLabel):
    def __init__(self, *args):
        super().__init__(*args)
        self.setAlignment(Qt.AlignCenter)


class QLabelSunken(QLabel):
    def __init__(self, *args):
        super().__init__(*args)
        self.setFrameStyle(QFrame.Panel | QFrame.Sunken)
        self.setAlignment(Qt.AlignRight | Qt.AlignVCenter)


class QLabelSunkenCentered(QLabel):
    def __init__(self, *args):
        super().__init__(*args)
        self.setFrameStyle(QFrame.Panel | QFrame.Sunken)
        self.setAlignment(Qt.AlignCenter)


class QLabelAlignedRight(QLabel):
    def __init__(self, *args):
        super().__init__(*args)
        self.setAlignment(Qt.AlignRight | Qt.AlignVCenter)


class QLineEditAlignedRight(QLineEdit):
    def __init__(self, *args):
        super().__init__(*args)
        self.setAlignment(Qt.AlignRight | Qt.AlignVCenter)


class QLineEditDisabled(QLineEdit):
    def __init__(self):
        super().__init__()
        self.setDisabled(True)


class QListWidgetAlternatingColors(QListWidget):
    def __init__(self):
        super().__init__()
        self.setAlternatingRowColors(True)


class QSingleShotTimer(QTimer):
    def __init__(self, parent):
        super().__init__(parent)
        self.setSingleShot(True)
